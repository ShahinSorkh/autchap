<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";
session_write_close();
header("content-type: application/json; charset=utf-8");

$search_term_pattern = "/.*/";
$search_term = isset($_REQUEST['s']) ? $_REQUEST['s'] : null;
$search_term = !is_null($search_term) && preg_match($search_term_pattern, $search_term) ? trim($search_term) : null;

if (is_null($search_term) || empty($search_term)) {
    $papers = Paper::find_by_cond(Paper::FIELD_TYPE."=".Paper::TYPE_PAPER, "RAND()", 5);
} else {
    $search_term = $db->real_escape_string($search_term); // escape string on $search_term
    $where_clause = Paper::FIELD_TITLE." LIKE '%".$search_term."%'";
    $where_clause .= " OR ".Paper::FIELD_WRITER." LIKE '%".$search_term."%'";
    $where_clause .= " OR ".Paper::FIELD_FIELD." LIKE '%".$search_term."%'";

    $papers = Paper::find_by_cond("({$where_clause}) AND ".Paper::FIELD_TYPE."=".Paper::TYPE_PAPER);
}
$number_of_papers = count($papers);
?>

{
    "papers": [

<?php if ($papers): ?>
    <?php for ($i = 0; $i < $number_of_papers; $i++): ?>
        {
        "id":<?= $papers[$i]->id ?>,
        "title":"<?= $papers[$i]->title ?>",
        "field":"<?= $papers[$i]->field ?>",
        "price":<?= $papers[$i]->calculate_price() ?>,
        "writer":"<?= $papers[$i]->writer ?>",
        "total_pages":<?= $papers[$i]->pages ?>,
        "paper_size":<?= $papers[$i]->size ?>,
        "description":"<?= preg_replace("/\\s+/", " ", nl2br($papers[$i]->desc)) ?>",
        "pic":"<?= $papers[$i]->pic ?>"
        }<?= ($i + 1 != $number_of_papers) ? "," : ""; ?>
    <?php endfor; ?>
<?php endif; ?>

    ]
}