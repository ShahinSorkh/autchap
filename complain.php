<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */
require_once __DIR__."/include/init.php";
session_write_close();

if (isset($_POST['submit']) && $_POST['submit'] == 'submit') {
    $name = isset($_POST['name']) ? $_POST['name']:false;
    $mobile = isset($_POST['mobile']) ? $_POST['mobile']:false;
    $subject = isset($_POST['subject']) ? $_POST['subject']:false;
    $text = isset($_POST['text']) ? $_POST['text']:false;

    if (!$name || empty($name)) $errors[] = 'نام و نام خانوادگی خود را وارد کنید';
    if (!$mobile || !preg_match('/^09[0-9]{9}$/', $mobile)) $errors[] = 'شماره همراه خود را وارد کنید';
    if (!$subject || empty($subject)) $errors[] = 'موضوع شکایت خود را وارد کنید';
    if (!$text || empty($text)) $errors[] = 'متن شکایت خود را وارد کنید';

    if (isset($errors) && !empty($errors))
        return print sprintf('<ul class="w3-ul"><li>%s</li></ul>', join('</li><li>', $errors));

    $fields = array(
        Complain::FIELD_NAME, Complain::FIELD_MOBILE,
        Complain::FIELD_TEXT, Complain::FIELD_CREATED
    );
    $values = array(
        "'{$name}'", "'{$mobile}'",
        "'{$text}'", "'".$db->to_formatted_time()."'"
    );

    $query = "INSERT INTO ".Complain::TABLE_NAME." (".join(',', $fields).") ";
    $query .= "VALUES (".join(',', $values).")";
    if ($db->query($query)) {
        return print 'با موفقیت ثبت شد. ممنون که نظر خود را با ما در میان می‌گذارید.';
    } else {
        Log::add(Log::ACTION_ERROR_DB, $db->error);
        return print 'خطایی رخ داد، لطفا از طریق support@autchap.ir با ما در تماس باشید.';
    }
}
exit();
