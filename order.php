<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";

if (isset($_POST['submit']) && $_POST['submit'] == 'submit') {
    $bookid = isset($_POST['bookid'])
        ? (int)$_POST['bookid'] : null;
    $quantity = isset($_POST['quantity'])
        ? (int)$_POST['quantity'] : null;
    $mobile = isset($_POST['mobile'])
        ? $_POST['mobile'] : null;
    $simi = isset($_POST['simi']) && $_POST['simi'] == "on"
        ? 1 : 0;

    if (is_null($bookid) || !preg_match('/^\d+$/', $bookid))
        $errors[] = "خطای نامشخصی رخ داد، لطفا دوباره سعی کنید.";
    if (is_null($quantity) || !preg_match('/^[1-9]\d*$/', $quantity))
        $errors[] = "تعداد مشخص شده نامعتبر است.";
    if (is_null($mobile) || !preg_match('/^09\d{9}$/', $mobile))
        $errors[] = "همراه وارد شده نامعتبر است.";

    if (isset($errors) && !empty($errors))
        redirect_to("index.php", "<ul class=\"w3-container\"><li>".join("</li><li>", $errors)."</li></ul>");

    $paper_type = Paper::find_by_id($bookid);
    if ($paper_type && $paper_type->type == Paper::TYPE_BOOK) $simi = 1;

    // everything is ok now the purchase should save
    $fields = array(
        Purchase::FIELD_BOOK, Purchase::FIELD_QUANTITY, Purchase::FIELD_MOBILE,
        Purchase::FIELD_STATUS, Purchase::FIELD_SIMI, Purchase::FIELD_DATE
    );
    $query = "INSERT INTO ".Purchase::TABLE_NAME." (".join(',', $fields).") VALUES (";
    $query .= "{$bookid},{$quantity},'{$mobile}','".Purchase::STATUS_SUBMITTED."',{$simi},'{$db->to_formatted_time()}')";
    if (!$db->query($query)) { // saving to database failed due an error
        $message = "ذخیره سازی در بانک اطلاعاتی با مشکل مواجه شد،"
            ." لطفا با پشتیبانی ما در میان بگذارید: support@autchap.ir<br/>".$db->get_error();
        redirect_to("index.php", $message);
    } else { // has been save to database successfully
        $purchase = Purchase::find_by_id($db->get_inserted_id());
        // request payment
        $payment = new Payment();
        $payment_key = $payment->request_payment($purchase->calculate_price(), $purchase->id, $purchase->book);
        // handle if request failed
        if ($payment_key < 0) {
            $purchase->remove();
            redirect_to("index.php", $payment->get_error($payment_key));
        }
        // redirect to bank portal via JavaScript
        get_session_message($msg);
        include_html_head();
        ?>
        <body class="w3-blue-gray">

        <div class="w3-container w3-theme-dark w3-animate-bottom w3-center" onclick="this.style.display = 'none';"
             style="position: fixed;bottom: 0;width: 100%;cursor: pointer;z-index: 98;">
            <?= !empty($msg) ? "<p>{$msg}</p>" : "" ?>
        </div>

        <div class="w3-card-2 w3-margin-top w3-white center" style="direction: rtl;">
            <header class="w3-container w3-theme">
                <p>
                    درحال انتقال به درگاه بانک
                    <span class="w3-tag w3-theme-action w3-left w3-padding-4">
						مبلغ <?= $purchase->calculate_price() ?></span>

                </p>
            </header>
            <div class="w3-container">
                <p class="w3-row w3-half">لطفا شکیبا باشید...</p>
                <div class="w3-row cssload-container w3-padding-0 w3-margin-0 w3-quarter">
                    <ul class="cssload-flex-container">
                        <li><span class="cssload-loading"></span></li>
                    </ul>
                </div>
            </div>
        </div>

        <script>
            setTimeout(function () {
                window.location = "http://pardano.com/p/payment/<?= $payment_key ?>";
            }, 800);
        </script>

        </body>
        </html>
        <?php
    }
} else // not submitted via a form
    redirect_to("index.php");