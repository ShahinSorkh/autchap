<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";

if (isset($_POST['submit']) && $_POST['submit'] = "submit") {
    $id = isset($_POST['id'])
        ? (int)$_POST['id'] : null;
    $type = isset($_POST['type'])
        ? (int)$_POST['type'] : null;
    $size = isset($_POST['papersize'])
        ? (int)$_POST['papersize'] : null;
    $pages = isset($_POST['pages'])
        ? (int)$_POST['pages'] : null;
    $added_price = isset($_POST['additionprice'])
        ? (int)$_POST['additionprice'] : null;
    $title = isset($_POST['title'])
        ? trim($_POST['title']) : null;
    $writer = isset($_POST['writer'])
        ? trim($_POST['writer']) : null;
    $field = isset($_POST['field'])
        ? trim($_POST['field']) : null;
    $desc = isset($_POST['desc'])
        ? trim($_POST['desc']) : null;

    if (is_null($id) || !is_numeric($id))
        $errors[] = "خطای نامشخصی رخ داد لطفا دوباره سعی کنید";
    if (is_null($type) || !is_numeric($type))
        $errors[] = "خطای نامشخصی رخ داد لطفا دوباره سعی کنید";
    if (is_null($size) || !is_numeric($size))
        $errors[] = "خطای نامشخصی رخ داد لطفا دوباره سعی کنید";
    if (is_null($pages) || !is_numeric($pages) || $pages < 1)
        $errors[] = "تعداد صفحات نمیتواند کمتر از 1 باشد";
    if (is_null($added_price) || !is_numeric($added_price) || $added_price < 0)
        $errors[] = "قیمت مازاد باید عددی نامنفی باشد";
    if (is_null($title) || empty($title))
        $errors[] = "عنوان را وارد کنید";
    if (is_null($writer) || empty($writer))
        $errors[] = "مولف را وارد کنید";
    if (is_null($field) || empty($field))
        $errors[] = "رشته را وارد کنید";
    if (is_null($desc) || empty($desc))
        $errors[] = "توضیحات را وارد کنید";

    if (isset($errors) && !empty($errors))
        redirect_to("admin.php", "<ul class=\"w3-ul\"><li>".join("</li><li>", $errors)."</li></ul>");

    $title = $db->real_escape_string($title);
    $writer = $db->real_escape_string($writer);
    $field = $db->real_escape_string($field);
    $desc = $db->real_escape_string($desc);

    // everything is ok, update the paper
    $sets = array(
        Paper::FIELD_TYPE."={$type}",
        Paper::FIELD_SIZE."={$size}",
        Paper::FIELD_PAGES."={$pages}",
        Paper::FIELD_ADDED_PRICE."={$added_price}",
        Paper::FIELD_TITLE."='{$title}'",
        Paper::FIELD_WRITER."='{$writer}'",
        Paper::FIELD_FIELD."='{$field}'",
        Paper::FIELD_DESC."='{$desc}'"
    );
    $query = "UPDATE ".Paper::TABLE_NAME." SET ".join(" , ", $sets)." WHERE ".Paper::FIELD_ID."={$id}";

    if ($db->query($query))
        $message = "با موفقیت ویرایش شد";
    else
        $message = "خطا! لطفا خط زیر را با پشتیبانی ما "
            ."<a href=\"mailto:support@autchap.ir\">support@autchap.ir</a>"." در میان بگذارید<br/>"
            .$db->get_error();

    redirect_to("admin.php", $message);
} else redirect_to("admin.php");