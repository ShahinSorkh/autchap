<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * Database interface including all methods have been used through the project
 */
interface DatabaseInterface
{
    /**
     * @param string $sql
     * @return bool|mysqli_result
     */
    function query($sql);

    /**
     * @return string
     */
    function get_last_query();

    /**
     * @param mysqli $result_set
     * @return array
     */
    function fetch_assoc($result_set);

    /**
     * @param string $string
     * @return int unix timestamp
     */
    function to_timestamp($string);

    /**
     * @param int $timestamp unix timestamp
     * @return string
     */
    function to_formatted_time($timestamp);

    /**
     * @return string
     */
    function get_error();

    /**
     * @return int
     */
    function get_errno();

    /**
     * @param string $string
     * @return string
     */
    function real_escape_string($string);

    /**
     * @return mixed
     */
    function get_inserted_id();
}