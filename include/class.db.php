<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * Database handler
 */
class Database extends mysqli implements DatabaseInterface
{

    private $_last_query = "";

    public function __destruct()
    {
        $this->close();
    }

    /**
     * @param string $query
     *
     * @return bool|mysqli_result
     */
    public function query($query)
    {
        $this->_last_query = $query;
        return parent::query($query, MYSQLI_STORE_RESULT);
    }

    /**
     * @return string
     */
    public function get_last_query()
    {
        return $this->_last_query;
    }

    /**
     * @param $result_set
     *
     * @return array|null
     */
    public function fetch_assoc($result_set)
    {
        return mysqli_fetch_assoc($result_set);
    }

    /**
     * @param $string
     *
     * @return false|int
     */
    public function to_timestamp($string)
    {
        $date = substr($string, 0, 10);
        $time = substr($string, strpos(" ", $string) + 1);

        $date = preg_split("/\\d{2,4}/", $date);
        $time = preg_split("/\\d{2}/", $time);

        return mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]);
    }

    /**
     * @param null $timetimestamp
     *
     * @return false|string
     */
    public function to_formatted_time($timetimestamp = null)
    {
        $timetimestamp = isset($timetimestamp) ? $timetimestamp : time() + 60 * 60 * 3 + 60 * 30;
        return date("Y-m-j h:i:s", $timetimestamp);
    }

    /**
     * @return int
     */
    public function get_errno()
    {
        return $this->errno;
    }

    /**
     * @return string
     */
    public function get_error()
    {
        return $this->error;
    }

    /**
     * @return mixed
     */
    public function get_inserted_id()
    {
        return $this->insert_id;
    }
}

$db = new Database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
