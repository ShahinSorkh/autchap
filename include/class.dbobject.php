<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * Database object to handle database data
 */
abstract class DatabaseObject
{

    /**
     * Exact name of the table in the database
     */
    const TABLE_NAME = "";

    /**
     * Exact name of field id in the database
     */
    const FIELD_ID = "";

    /**
     * @var
     */
    public $id;

    /**
     * @param string|null $where_clause
     * @param string|null $order_by
     * @param string|null $limit
     *
     * @return int|false
     */
    public static function count_records($where_clause = null, $order_by = null, $limit = null)
    {
        global $db;

        $query = "SELECT COUNT(*) AS NumberOfRecords FROM ".static::TABLE_NAME;
        $query .= is_null($where_clause) ? "" : " WHERE {$where_clause}";
        $query .= is_null($order_by) ? "" : " ORDER BY {$order_by}";
        $query .= is_null($limit) ? "" : " LIMIT {$limit}";
        $result_set = $db->query($query);

        if ($result_set) {
            $row = $db->fetch_assoc($result_set);
            return $row['NumberOfRecords'];
        } else {
            $_SESSION['message'] = $db->get_error();
            return false;
        }
    }

    /**
     * @param int $id
     *
     * @return mixed|false db object or false if nothing found
     */
    public static function find_by_id($id)
    {
        $result_arr = static::find_by_cond(static::FIELD_ID."=".(int)$id);
        if ($result_arr != false)
            return array_shift($result_arr);
        else
            return false;
    }

    /**
     * find by where, order by, limit and offset options
     *
     * @param string $where_clause
     * @param string|null $order_by
     * @param string|null $limit
     * @param string|null $offset
     *
     * @return array|bool array of db objects or false if nothing found
     */
    public static function find_by_cond($where_clause, $order_by = null, $limit = null, $offset = null)
    {
        global $db;

        $query = "SELECT * FROM ".static::TABLE_NAME." WHERE {$where_clause}";
        $query .= $order_by != null ? " ORDER BY {$order_by}" : "";
        $query .= $limit != null ? " LIMIT {$limit}" : "";
        $query .= $offset != null ? " OFFSET {$offset}" : "";
        $result_set = $db->query($query);

        if ($result_set)
            return static::_instance($result_set);
        else {
            $_SESSION['message'] = $db->get_error();
            return false;
        }
    }

    /**
     * implemented to be used in DatabaseObject class
     *
     * @param mysqli_result $result_set
     *
     * @return array
     */
    private static function _instance($result_set)
    {
        global $db;

        $instances = array();
        while (($row = $db->fetch_assoc($result_set))) {
            $instance = new static();
            foreach (get_class_vars(static::class) as $key => $value) {
                $const = constant("static::FIELD_".strtoupper($key));
                if ($const == null) continue;
                $instance->$key = htmlspecialchars($row[$const]);
            }
            $instances[] = $instance;
        }
        return $instances;
    }

    /**
     * @param string|null $order_by
     *
     * @return array|false array of db objects or false if nothing found
     */
    public static function find_all($order_by = null)
    {
        $result_arr = static::find_by_cond("1=1", $order_by);

        if ($result_arr)
            return $result_arr;
        else
            return false;
    }

    /**
     * find by custom sql query
     *
     * @param string $sql
     *
     * @return array|false array of db objects or false if nothing found
     */
    protected static function find_by_sql($sql)
    {
        global $db;

        $result_set = $db->query($sql);
        if ($result_set) {
            return static::_instance($result_set);
        } else {
            $_SESSION['message'] = $db->get_error();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function remove()
    {
        global $db;

        $query = "DELETE FROM ".static::TABLE_NAME." WHERE ".static::FIELD_ID."={$this->id}";
        return $db->query($query);
    }

}