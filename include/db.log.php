<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * Log table objects
 */
class Log extends DatabaseObject
{
    const TABLE_NAME = "log";
    // table fields
    const FIELD_ID = "logid";
    const FIELD_IP = "ipaddress";
    const FIELD_SESSION = "sessionid";
    const FIELD_ACTION = "action";
    const FIELD_DESC = "description";
    const FIELD_TIMESTAMP = "unixtime";
    // possible actions
    const ACTION_LOGIN_FAILED = "Login failed";
    const ACTION_LOGIN = "Login";
    const ACTION_ERROR_404 = "Error 404";
    const ACTION_ERROR_DB = "Database error";

    public $id, $ip, $session, $action, $desc, $timestamp;

    /**
     * @param string $action
     * @param string $desc
     *
     * @return bool
     */
    public static function add($action, $desc)
    {
        global $db;

        $fields = array(
            self::FIELD_IP,
            self::FIELD_SESSION,
            self::FIELD_ACTION,
            self::FIELD_DESC,
            self::FIELD_TIMESTAMP
        );
        $values = array(
            $db->real_escape_string(trim($_SERVER['REMOTE_ADDR'])),
            $db->real_escape_string(trim(session_id())),
            $db->real_escape_string(trim($action)),
            $db->real_escape_string(trim($desc))
        );
        $query = "INSERT INTO ".self::TABLE_NAME." (".join($fields, ", ")
            .") VALUES ('".join($values, "', '")."',".(time() + 60 * 60 * 3 + 60 * 30).")";

        return $db->query($query);
    }

    /**
     * @return false|int
     */
    public static function count_failed_logins()
    {
        global $db;

        $ip = $db->real_escape_string(trim($_SERVER['REMOTE_ADDR']));
        $session_id = $db->real_escape_string(trim(session_id()));

        $where_clause = self::FIELD_ACTION."='".self::ACTION_LOGIN_FAILED."' AND ("
            ."SELECT ".self::FIELD_ID." FROM ".self::TABLE_NAME
            ." WHERE ".self::FIELD_ACTION."='".self::ACTION_LOGIN."'"
            ." ORDER BY ".self::FIELD_TIMESTAMP." DESC LIMIT 1"
            .") < logid AND ("
            .self::FIELD_IP."='{$ip}' OR ".self::FIELD_SESSION."='{$session_id}')";

        return self::count_records($where_clause, self::FIELD_ID." DESC");
    }

    /**
     * @param string|null $ip
     * @param string|null $session_id
     *
     * @return Log
     */
    public static function find_last_failed_attempt($ip = null, $session_id = null)
    {
        global $db;

        $ip = is_null($ip) ? $_SERVER['REMOTE_ADDR'] : $ip;
        $ip = $db->real_escape_string(trim($ip));

        $session_id = is_null($session_id) ? session_id() : $session_id;
        $session_id = $db->real_escape_string(trim($session_id));

        $where_clause = self::FIELD_ACTION."='".self::ACTION_LOGIN_FAILED."'";
        $where_clause .= " AND (".self::FIELD_IP."='{$ip}' OR ".self::FIELD_SESSION."='{$session_id}')";

        return self::find_by_cond($where_clause, self::FIELD_ID." DESC", 1)[0];
    }
}