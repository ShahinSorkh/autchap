<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * The middle man between the app and the bank
 */
class Payment extends SoapClient
{

    /**
     * SOAP connection target
     */
    const WSDL = "http://pardano.com/p/webservice/?wsdl";

    /**
     * API number has gotten from the bank
     */
    const API = "tNBUq";

    /**
     * Possible errors from the bank
     */
    const ERRORS = array(
        -1 => 'api نامعتبر است',
        -2 => 'مبلغ از کف تعریف شده کمتر است',
        -3 => 'مبلغ از سقف تعریف شده بیشتر است',
        -4 => 'مبلغ نامعتبر است',
        -6 => 'درگاه غیرفعال است',
        -7 => 'آی پی شما مسدود است',
        -9 => 'آدرس کال بک خالی است',
        -10 => 'چنین تراکنشی یافت نشد',
        -11 => 'تراکنش انجام نشده',
        -12 => 'تراکنش انجام شده اما مبلغ نادرست است',
    );

    /**
     * Payment constructor.
     */
    public function __construct()
    {
        parent::SoapClient(self::WSDL);
    }

    /**
     * @param string $price
     * @param string $order_id
     * @param string $description
     * @param bool $is_donate
     *
     * @return int error code or payment request code
     */
    public function request_payment($price, $order_id, $description, $is_donate = false)
    {
        $callback = $is_donate ? SITE_HOME."/pay.php?come_from=bank":SITE_HOME."/bank_response.php?come_from=bank";
        return $this->__soapCall("requestpayment", array(
            "api" => self::API,
            "price" => $price,
            "callback" => $callback,
            "orderid" => $order_id,
            "description" => $description
        ));
    }

    /**
     * @param string $price
     * @param string $authority
     *
     * @return int error code or 1 as ok result
     */
    public function verification($price, $authority)
    {
        return $this->__soapCall("verification", array(
            "api" => self::API,
            "price" => $price,
            "au" => $authority
        ));
    }

    /**
     *
     * I have no idea what is this going to do!
     *
     * @param string $username
     * @param string $password
     * @param string $to
     * @param string $price
     *
     * @return int [probably]
     */
    public function transfer($username, $password, $to, $price)
    {
        return $this->__soapCall("transfer", array(
            "username" => $username,
            "password" => $password,
            "to" => $to,
            "price" => $price
        ));
    }

    /**
     * @param int $err
     *
     * @return string|false
     */
    public function get_error($err = null)
    {
        if (!is_numeric($err) || $err > -1 || $err < -12) return false;
        return self::ERRORS[$err];
    }
}