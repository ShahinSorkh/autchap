<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * Paper table objects
 */
class Paper extends DatabaseObject
{
    const TABLE_NAME = "paper";
    // fields
    const FIELD_ID = "idpaper";
    const FIELD_TITLE = "title";
    const FIELD_FIELD = "field";
    const FIELD_PAGES = "total_pages";
    const FIELD_WRITER = "writer";
    const FIELD_DESC = "description";
    const FIELD_TYPE = "papertype";
    const FIELD_SIZE = "papersize";
    const FIELD_ADDED_PRICE = "additionprice";
    const FIELD_PIC = "pic";
    // possible paper sizes
    const SIZE_A4 = 0;
    const SIZE_A5 = 1;
    // possible paper types
    const TYPE_BOOK = 1;
    const TYPE_PAPER = 2;

    public $id, $title, $field, $pages, $writer, $desc, $type, $size, $added_price, $pic;

    /**
     * @return Paper
     */
    static function get_blank()
    {
        $new = new self();
        $vars = get_class_vars(self::class);
        foreach ($vars as $var => $value) $new->$var = "";
        $new->size = self::SIZE_A4;
        $new->type = self::TYPE_BOOK;
        $new->pages = 0;
        $new->added_price = 0;
        return $new;
    }

    /**
     * @return bool
     */
    public function remove()
    {
        return parent::remove() && unlink(__DIR__."/../uploads/pic/".$this->pic);
    }

    /**
     * @return int
     */
    public function calculate_price()
    {
        global $options;

        $divide = 1;
        switch ($this->size) {
            case self::SIZE_A4:
                $divide = 2;
                break;
            case self::SIZE_A5:
                $divide = 4;
                break;
        }
        $prices = array(
            "paper" => $options->get_price(Options::ITEM_PRICE_PAPER),
            "addition" => $options->get_price(Options::ITEM_PRICE_ADDITION)
        );

        return round_int($this->pages / $divide * $prices['paper'] + $prices['addition'] + $this->added_price, 50);
    }

    /**
     * @return string
     */
    public function get_paper_size()
    {
        $out = "";
        switch ($this->size) {
            case self::SIZE_A4:
                $out = "A4";
                break;
            case self::SIZE_A5:
                $out = "A5";
                break;
        }
        return $out;
    }
}