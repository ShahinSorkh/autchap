<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * Purchase table objects
 */
class Purchase extends DatabaseObject
{
    const TABLE_NAME = "purchase";
    // fields
    const FIELD_ID = "purchaseid";
    const FIELD_BOOK = "bookid";
    const FIELD_MOBILE = "mobile";
    const FIELD_QUANTITY = "quantity";
    const FIELD_SIMI = "simi";
    const FIELD_AUTH = "authority";
    const FIELD_PAID = "paid";
    const FIELD_STATUS = "status";
    const FIELD_DATE = "date";
    // possible statuses
    const STATUS_WAITING = "در دست بررسی";
    const STATUS_DONE = "تحویل شده";
    const STATUS_SUBMITTED = "ثبت شده";

    public $id, $book, $mobile, $quantity, $simi, $auth, $paid, $status, $date;

    /**
     * @param int $auth
     *
     * @return bool
     */
    public function paid($auth, $price)
    {
        global $db;

        $auth = filter_var($auth, FILTER_VALIDATE_INT);
        $price = filter_var($price, FILTER_VALIDATE_INT);

        if (!$auth || !$price)
            return false;

        $query = "UPDATE ".self::TABLE_NAME." SET ";
        $query .= self::FIELD_AUTH."={$auth},".self::FIELD_PAID."={$price}";
        $query .= " WHERE ".self::FIELD_ID."={$this->id}";

        return $db->query($query) && $this->set_status('waiting');
    }

    /**
     * @param string|null $status
     *
     * @return bool
     */
    public function set_status($status = null)
    {
        if (is_null($status)) return false;
        global $db;

        $query = "UPDATE ".self::TABLE_NAME." SET ".self::FIELD_STATUS."='";
        switch ($status) {
            case 'waiting':
                $query .= self::STATUS_WAITING;
                break;
            case 'done':
                $query .= self::STATUS_DONE;
                break;
            default:
                return false;
        }
        $query .= "' WHERE ".self::FIELD_ID."= {$this->id}";

        return $db->query($query);
    }

    /**
     * @return bool
     */
    public function check()
    {
        return $this->set_status('done');
    }

    /**
     * @return int
     */
    public function calculate_price()
    {
        global $options;
        $paper = Paper::find_by_id($this->book);

        $price_of_simi = $this->simi ? $options->get_price(Options::ITEM_PRICE_SIMI) : 0;
        $price_per_paper = $paper->calculate_price() + $price_of_simi;
        return $price_per_paper * $this->quantity;
    }
}