<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * Class Options
 */
class Options
{
    /**
     * Absolute path to options.json
     */
    const JSON_PATH = __DIR__."/../options.json";
    // Items available in options.json
    const ITEM_PRICE_SIMI = "price_simi";
    const ITEM_PRICE_ADDITION = "price_addition";
    const ITEM_PRICE_PAPER = "price_paper";

    /**
     * @var object JSON object
     */
    private $_obj;

    /**
     * Options constructor.
     */
    public function __construct()
    {
        $json_file = file_get_contents(self::JSON_PATH);
        $this->_obj = json_decode($json_file);
    }

    /**
     * @param $item
     * @return int
     */
    public function get_price($item)
    {
        switch ($item) {
            case self::ITEM_PRICE_ADDITION:
                return $this->_obj->price_addition;
            case self::ITEM_PRICE_PAPER:
                return $this->_obj->price_paper;
            case self::ITEM_PRICE_SIMI:
                return $this->_obj->price_simi;
            default:
                return 0;
        }
    }

    /**
     * @param $addition
     * @param $paper
     * @param $simi
     * @return bool
     */
    public function set_prices($addition, $paper, $simi)
    {
        if (filter_var($addition, FILTER_VALIDATE_INT) === false
            || filter_var($paper, FILTER_VALIDATE_INT) === false
            || filter_var($simi, FILTER_VALIDATE_INT) === false
        ) return false;

        $lines = file(self::JSON_PATH);
        for ($i = 0; $i < count($lines); $i++) {
            if (strpos($lines[$i], self::ITEM_PRICE_PAPER)) {
                $lines[$i] = "\"".self::ITEM_PRICE_PAPER."\": {$paper}";
            } elseif (strpos($lines[$i], self::ITEM_PRICE_ADDITION)) {
                $lines[$i] = "\"".self::ITEM_PRICE_ADDITION."\": {$addition}";
            } elseif (strpos($lines[$i], self::ITEM_PRICE_SIMI)) {
                $lines[$i] = "\"".self::ITEM_PRICE_SIMI."\": {$simi}";
            }
            $lines[$i] .= ($i > 0 && $i < count($lines) - 2) ? ",\n":"\n";
        }
        $out = preg_replace('/\n+/', "\n", implode($lines));
        return is_numeric(file_put_contents(self::JSON_PATH, $out));
    }

}

$options = new Options();
