<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

// database core
require_once __DIR__."/db_config_somestupidhashsothatthefuckinghackercannotguess.php";
require_once __DIR__."/interface.db.php";
require_once __DIR__."/class.db.php";
require_once __DIR__."/class.dbobject.php";
require_once __DIR__."/class.payment.php";
// database objects
require_once __DIR__."/db.log.php";
require_once __DIR__."/db.paper.php";
require_once __DIR__."/db.purchase.php";
require_once __DIR__."/db.pay.php";
require_once __DIR__."/db.complain.php";
// helpers
require_once __DIR__."/functions.php";
require_once __DIR__."/class.options.php";

if (empty(session_id())) session_start();
