<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * @param string $title
 * @param array $js
 */
function include_html_head($title = "", $js = null)
{
    $out = "<!doctype html><html lang=\"fa\"><head>\n<meta charset=\"UTF-8\" />";
    $out .= "<meta name=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0\" />";
    $out .= "<meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\" />";
    $out .= "<meta name=\"samandehi\" content=\"251324618\" />\n";
    $out .= "<title>" . (!empty($title) ? "{$title} | " : "") . "AUT chap</title>\n";
    $out .= "<link rel=\"icon\" href=\"" . SITE_HOME . "/img/favico.png\" />";
    $out .= "<link rel=\"apple-touch-icon\" href=\"" . SITE_HOME . "/img/favico.png\" />";
    if (empty(DEBUG)) { // debug mode
        $out .= "<script src=\"" . SITE_HOME . "/js/jquery.js\"></script>\n";
        $out .= "<link rel=\"stylesheet\" href=\"" . SITE_HOME . "/css/font-awesome.min.css\"/>\n";
    } else { // production mode
        $out .= "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>\n";
        $out .= "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs"
            . "/font-awesome/4.7.0/css/font-awesome.min.css\" />\n";
    }
    $out .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . SITE_HOME . "/css/style.min.css?v=1.3\" />";

    if (is_array($js) && !empty($js)) {
        foreach ($js as $script) {
            $out .= "<script src=\"" . SITE_HOME . "/js/{$script}" . DEBUG . ".js?v=1.3\"></script>";
        }
    }

    $out .= "\n</head>";
    echo $out;
}

/**
 * @param $msg resource the variable to save session message in
 */
function get_session_message(&$msg)
{
    $msg = isset($_SESSION['message']) ? $_SESSION['message'] : "";
    unset($_SESSION['message']);
}

/**
 * @param string|null $location
 * @param string $message
 */
function redirect_to($location = null, $message = "")
{
    if (!is_null($location)) {
        empty($message) ? null : $_SESSION['message'] = $message;
        header("Location: {$location}");
        exit();
    }
}

/**
 * @param float $val
 * @param int $precision 10^n [+ 5*10^m (n>m)]
 * @param int $flag PHP_ROUND_HALF_UP or PHP_ROUND_HALF_DOWN
 *
 * @return int
 */
function round_int($val, $precision, $flag = PHP_ROUND_HALF_UP)
{
    $val = (int)$val;
    $tens = 10;
    while ($precision > $tens) {
        $tens *= 10;
    }
    $reminder = $val % $tens;
    $round = $val - $reminder;
    switch ($flag) {
        case PHP_ROUND_HALF_UP:
            if ($reminder >= $precision)
                return $round + $tens;
            elseif ($reminder > 0)
                return $round + $precision;
            return $round;
        case PHP_ROUND_HALF_DOWN:
            if ($reminder >= $precision)
                return $round + $precision;
            elseif ($reminder > 0)
                return $round;
            return $round;
        default:
            return $round;
    }
}
