<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/**
 * Pay table objects
 */
class Pay extends DatabaseObject {
    const TABLE_NAME = "pay";
    // fields
    const FIELD_ID = "idpay";
    const FIELD_AUTH = "auth";
    const FIELD_AMOUNT = "amount";
    const FIELD_DESC = "description";
    const FIELD_DATE = "date";

    public $id, $auth, $amount, $desc, $date;

    /**
     * @param int $au
     * @return bool
     */
    public function paid($au) {
        global $db;

        $au = (int) $au;
        if (is_numeric($au)) {
            $query = "UPDATE ".self::TABLE_NAME." SET ".self::FIELD_AUTH."={$au} WHERE ".self::FIELD_ID."={$this->id}";
            return $db->query($query);
        }
        return false;
    }
}