<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

class Complain extends DatabaseObject
{
    const TABLE_NAME = "complain";

    const FIELD_ID = "complainid";
    const FIELD_NAME = "flname";
    const FIELD_MOBILE = "mobile";
    const FIELD_TEXT = "ttext";
    const FIELD_CREATED = "created";

    public $id, $name, $mobile, $text, $created;
}
