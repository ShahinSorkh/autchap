<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__ . "/include/init.php";
// if not logged in pass to login.php
if (!isset($_SESSION['uid'])) redirect_to("login.php", "ابتدا باید وارد شوید");

$purchases = Purchase::find_by_cond(Purchase::FIELD_AUTH . " IS NOT NULL", Purchase::FIELD_ID . " DESC");

get_session_message($msg);
include_html_head("Orders");
?>

<body>

<nav id="myNav" class="w3-topnav w3-theme w3-padding w3-card-4" style="direction: rtl;width: 100%;z-index: 99">
    <a class="w3-right" href="index.php"><i class="fa fa-home"></i> خانه</a>
    <a class="w3-right" href="orders_list.php">داشبورد</a>
    <a class="w3-right" href="pay.php#all-pays">پرداخت‌ها</a>
    <a class="w3-left" href="login.php?do=logout">خروج <i class="fa fa-sign-out"></i></a>
</nav>
<div class="w3-container w3-theme-dark w3-animate-bottom w3-center w3-bottom" onclick="$(this).slideUp()"
     style="width: 100%;cursor: pointer;z-index: 98;direction: rtl">
    <?= !empty($msg) ? "<p>{$msg}</p>" : "" ?>
</div>

<div class="w3-container" style="direction: rtl">
    <table class="w3-table-all w3-border w3-center w3-margin-top w3-margin-bottom w3-card-2" style="width: 100%;">
        <tr class="w3-theme">
            <th>ردیف</th>
            <th>نام کتاب/جروه</th>
            <th>تعداد</th>
            <th>سیمی</th>
            <th>شماره همراه خریدار</th>
            <th>کدپیگیری</th>
            <th>مبلغ پرداختی (تومان)</th>
        </tr>
        <?php
        $i = 0;
        $total_price = 0;
        $total_prints = 0;
        foreach ($purchases as $purchase):
            $paper = Paper::find_by_id($purchase->book);
            ?>
            <tr class="">
                <td><?= ++$i ?></td>
                <td class="w3-right-align"><?= $paper ? $paper->title : "<em>حذف شده</em>" ?></td>
                <td><?= $purchase->quantity ?></td>
                <td><?= $purchase->simi ? "&radic;" : "" ?></td>
                <td><?= $purchase->mobile ?></td>
                <td><?= $purchase->auth ?></td>
                <td><?= $purchase->paid ?></td>
            </tr>
            <?php
            $total_price += $purchase->paid;
            $total_prints += $purchase->quantity;
        endforeach;
        ?>
        <tr class="w3-theme">
            <th colspan="2"></th>
            <th><?= $total_prints ?></th>
            <th colspan="3"></th>
            <th><?= $total_price ?></th>
        </tr>
    </table>
</div>

</body>
</html>
