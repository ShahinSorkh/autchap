<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";

if (isset($_REQUEST['come_from']) && $_REQUEST['come_from'] == 'bank') {
    $orderid = isset($_REQUEST['order_id'])
        ? filter_var($_REQUEST['order_id'], FILTER_VALIDATE_INT) : false;

    if ($orderid !== false) {
        $order = Purchase::find_by_id($orderid);
        if (isset($_REQUEST['au'])) { // everything is ok, check the payment
            $au = filter_var($_REQUEST['au'], FILTER_VALIDATE_INT);
            if ($au === false)
                redirect_to("index.php", "خطایی رخ داد، مبلغ کسر شده ظرف ۷۲ ساعت به حساب شما بازمیگردد");

            $payment = new Payment();
            $res = $payment->verification($order->calculate_price(), $au);

            if ($res === 1 || $res === '1') {
                $order->set_status('waiting');
                $order->paid($au, $order->calculate_price());
                $_SESSION['purchase_done'] = $orderid;
                redirect_to("index.php#thanks_for_shopping", "سفارش با موفقیت کامل شد، منتظر تماس ما باشید");
            } else {
                $order->remove();
                redirect_to("index.php", $payment->get_error($res));
            }

        } else { // payment canceled by user
            $order->remove();
            redirect_to("index.php", "سفارش لغو شد<br/>درصورت وجود هرگونه اشتباه "
                ."با پشتیبانی ما در میان بگذارید"
                ." <a href=\"mailto:support@autchap.ir\">support@autchap.ir</a>");
        }
    }
}

redirect_to("index.php");