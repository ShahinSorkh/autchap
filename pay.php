<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";

if (isset($_REQUEST['come_from']) && $_REQUEST['come_from'] == "bank") { // check payment
    $orderid = isset($_REQUEST['order_id'])
        ? filter_var($_REQUEST['order_id'], FILTER_VALIDATE_INT) : false;

    if ($orderid !== false) {
        $order = Pay::find_by_id($orderid);
        if (isset($_REQUEST['au'])) { // everything is ok, check the payment
            $au = filter_var($_REQUEST['au'], FILTER_VALIDATE_INT);
            if ($au === false)
                redirect_to("index.php", "خطایی رخ داد، مبلغ کسر شده ظرف ۷۲ ساعت به حساب شما بازمیگردد");

            $payment = new Payment();
            $res = $payment->verification($order->amount, $au);

            if ($res === 1 || $res === '1') {
                $order->paid($au);
                echo "seccess";
                $_SESSION['payment_done'] = $orderid;
                redirect_to("pay.php#thanks_for_shopping", "سفارش با موفقیت کامل شد، منتظر تماس ما باشید");
            } else {
                $order->remove();
                echo "error";
                redirect_to("pay.php", $payment->get_error($res));
            }

        } else { // payment canceled by user
            $order->remove();
            redirect_to("pay.php", "پرداخت لغو شد<br/>درصورت وجود هرگونه اشتباه "
                ."با پشتیبانی ما در میان بگذارید"
                ." <a href=\"mailto:support@autchap.ir\">support@autchap.ir</a>");
        }
    }
} elseif (isset($_POST['submit']) && $_POST['submit'] == 'submit') { // send user to bank portal
    if (!isset($_POST['name']) || $_POST['name'] != session_id())
        redirect_to("index.php"); // form has been submitted via a robot

    $amount = isset($_POST['amount']) ? (int)$_POST['amount'] : null;
    $desc = isset($_POST['desc']) ? trim($_POST['desc']) : null;

    if (is_null($amount) || !is_numeric($amount) || $amount < 100)
        $errors[] = "مبلغ باید بیشتر از 100 تومان باشد";
    if (is_null($desc) || empty($desc))
        $errors[] = "حتما نام یا شماره همراه خود را وارد کنید";
    if (isset($errors) && !empty($errors))
        redirect_to("pay.php", "<ul class=\"w3-ul\"><li>".join("</li><li>", $errors)."</li></ul>");

    $fields = array(Pay::FIELD_AMOUNT, Pay::FIELD_DESC, Pay::FIELD_DATE);
    $values = array($amount, "'{$desc}'", "'{$db->to_formatted_time(time() + 60 * 60 * 3 + 60 * 30)}'");
    $query = "INSERT INTO ".Pay::TABLE_NAME." (".join(",", $fields).") VALUES (".join(",", $values).")";
    if ($db->query($query)) {
        $payment = new Payment();
        $res = $payment->request_payment($amount, $db->get_inserted_id(), $desc, true);
        if ($res > 0)
            redirect_to("http://pardano.com/p/payment/{$res}");
        else
            redirect_to("pay.php", $payment->get_error($res));
    } else {
        Log::add(Log::ACTION_ERROR_DB, $db->get_error());
        redirect_to("pay.php", "متاسفانه پرداخت با مشکل مواجه شد، لطفا بعدا دوباره امتحان کنید");
    }
} else { // show payment form
    $amount = isset($_REQUEST['amount'])
        ? filter_var((int)$_REQUEST['amount'], FILTER_VALIDATE_INT) : false;
    $amount = !$amount || $amount < 100
        ? 100 : round_int($amount, 10);

    get_session_message($msg);
    include_html_head("Pay");
    ?>
    <body>
    <div class="w3-blue-gray" style="position: fixed;z-index: -99; height: 100%;width: 100%;"></div>
    <div class="w3-container w3-theme-dark w3-animate-top w3-center" onclick="$(this).slideUp()"
         style="cursor: pointer;direction: rtl">
        <?= !empty($msg) ? "<p>{$msg}</p>" : "" ?>
    </div>

    <div class="w3-card-2 w3-margin-top w3-white center">
        <header class="w3-theme w3-container w3-large w3-right-align"><p>پرداخت</p></header>
        <div class="w3-container w3-form w3-right-align">
            <form method="post" action="pay.php">
                <input type="text" name="name" value="<?= session_id() ?>" class="w3-hide"/>
                <div class="w3-row w3-padding w3-right-align" style="direction: rtl">
                    <label class="w3-label" for="amount">مبلغ (تومان)</label>
                    <input type="number" step="50" id="amount" name="amount" value="<?= $amount ?>" min="100"
                           class="w3-input w3-border" style="width: 75%;" required/>
                </div>
                <div class="w3-row w3-padding w3-right-align" style="direction: rtl">
                    <label class="w3-label" for="desc">توضیح (نام یا شماره همراه خود را ذکر کنید)</label>
                    <textarea id="desc" name="desc" class="w3-border w3-input w3-animate-input" required></textarea>
                </div>
                <div class="w3-row w3-padding" style="direction: rtl">
                    <button type="submit" value="submit" name="submit" class="w3-btn w3-theme-action">پرداخت</button>
                </div>
            </form>
        </div>
        <footer class="w3-theme w3-container w3-right-align">
            <p class="w3-left-align" style="direction: rtl">
                <a href="index.php">بازگشت<i class="fa fa-angle-double-left"></i></a>
            </p>
        </footer>
    </div>

    <?php
    if (isset($_SESSION['payment_done'])) {
        $done_payment_id = filter_var($_SESSION['payment_done'], FILTER_VALIDATE_INT);
        if ($done_payment_id != false):
            $done_payment = Pay::find_by_id($done_payment_id); ?>
            <div class="w3-container w3-margin-top center">
                <div class="w3-card-2 w3-yellow w3-margin-bottom" style="direction: rtl">
                    <header class="w3-theme w3-container">
                        <p class="w3-large">نتیجه پرداخت <span class="w3-tag">پرداخت با موفقیت ثبت شد</span></p>
                    </header>
                    <div class="w3-container">
                        <p>
                            با موفقیت مبلغ
                            <span class="w3-tag"><?= $done_payment->amount ?> تومان</span>
                            پرداخت کردید.
                        </p>
                        <p class="w3-center w3-xlarge">
                            کد رهگیری:
                            <span class="w3-tag w3-green w3-jumbo"><?= $done_payment->auth ?></span>
                        </p>
                        <p class="w3-large">
                            متشکریم
                            <br/>
                            در صورت لزوم میتوانید با
                            <a href="mailto:support@autchap.ir">support@autchap.ir</a>
                            تماس بگیرید.
                        </p>
                    </div>
                    <footer class="w3-theme w3-container w3-right-align">
                        <p class="w3-left-align" style="direction: rtl">
                            <a id="thanks_for_shopping" href="index.php">بازگشت<i
                                    class="fa fa-angle-double-left"></i></a>
                        </p>
                    </footer>
                </div>
            </div>
            <?php
        endif;
        unset($_SESSION['payment_done']); // I'm done with this session data
    }
    ?>

    <?php // show pays to admin
    if (isset($_SESSION['uid'])):
        $all_pays = Pay::find_by_cond(Pay::FIELD_AUTH." IS NOT NULL", Pay::FIELD_ID." DESC");
        if ($all_pays): ?>
            <div class="w3-margin-top center" id="all-pays">
                <div class="w3-card-2 w3-margin-top w3-margin-bottom w3-white">
                    <header class="w3-theme w3-container"><p class="w3-right-align">همه پرداخت‌ها</p></header>
                    <ul class="w3-ul w3-container" style="direction: rtl">
                        <?php foreach ($all_pays as $pay): ?>
                            <li class="w3-hover-pale-blue">
                                <span class="w3-tag"><?= $pay->auth ?></span>
                                <span class="w3-tag w3-green"><?= $pay->amount ?> تومان</span>
                                <span class="w3-container"><?= $pay->desc ?></span>
                                <span class="w3-left"><?= $pay->date ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <footer class="w3-theme w3-container w3-right-align">
                        <p class="w3-left-align" style="direction: rtl">
                            <a href="admin.php">پرتال<i class="fa fa-angle-double-left"></i></a>
                        </p>
                    </footer>
                </div>
            </div>
        <?php else: ?>
            <div class="w3-container w3-margin-top center w3-theme-action w3-right-align"><p>پرداختی صورت نگرفته</p>
            </div>
            <?php
        endif;
    endif; ?>

    </body>
    </html>
<?php } ?>
