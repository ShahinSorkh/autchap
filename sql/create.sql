CREATE TABLE IF NOT EXISTS paper (
  idpaper       INT(11)              NOT NULL AUTO_INCREMENT,
  title         VARCHAR(120)         NOT NULL,
  field         VARCHAR(60)          NOT NULL,
  total_pages   INT(11)              NOT NULL,
  writer        VARCHAR(60)          NOT NULL,
  description   TEXT                 NOT NULL,
  papertype     TINYINT(1)           NOT NULL,
  papersize     TINYINT(1)           NOT NULL,
  pic           VARCHAR(120),
  additionprice SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`idpaper`)
);

CREATE UNIQUE INDEX pic ON paper (pic);

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchaseid` INT(11)     NOT NULL AUTO_INCREMENT,
  `bookid`     INT(11)     NOT NULL,
  `mobile`     VARCHAR(11) NOT NULL,
  `quantity`   INT(11)     NOT NULL,
  `simi`       TINYINT(1)  NOT NULL,
  `authority`  INT(11)              DEFAULT NULL,
  `paid`       INT(11)              DEFAULT NULL,
  `status`     VARCHAR(25) NOT NULL,
  `date`       TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`purchaseid`),
  FOREIGN KEY (`bookid`) REFERENCES `paper` (`idpaper`)
);

CREATE TABLE IF NOT EXISTS `log` (
  `logid`       INT         NOT NULL AUTO_INCREMENT,
  `ipaddress`   VARCHAR(15) NOT NULL,
  `sessionid`   VARCHAR(26) NOT NULL,
  `action`      TEXT        NOT NULL,
  `description` TEXT        NOT NULL,
  `unixtime`    INT         NOT NULL,
  PRIMARY KEY (`logid`)
);

CREATE TABLE IF NOT EXISTS `pay` (
  `idpay`       INT(11)   NOT NULL AUTO_INCREMENT,
  `auth`        INT(11)            DEFAULT NULL,
  `amount`      INT(11)   NOT NULL,
  `description` TEXT      NOT NULL,
  `date`        TIMESTAMP NOT NULL,
  PRIMARY KEY (`idpay`)
);

CREATE TABLE IF NOT EXISTS `complain`
(
  `complainid` INT          NOT NULL AUTO_INCREMENT,
  `flname`     VARCHAR(120) NOT NULL,
  `mobile`     VARCHAR(11)  NOT NULL,
  `ttext`      TEXT         NOT NULL,
  `created`    TIMESTAMP    NOT NULL,
  PRIMARY KEY (`complainid`)
);
