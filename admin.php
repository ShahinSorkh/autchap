<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";
// if not logged in pass to login.php
if (!isset($_SESSION['uid'])) redirect_to("login.php", "ابتدا باید وارد شوید");

if (isset($_POST['submit']) && $_POST['submit'] == "edit") {
    $id = isset($_POST['id']) ? (int)$_POST['id'] : null;
    $edit_paper = Paper::find_by_id($id);
    unset($_POST['submit']);
} else $edit_paper = Paper::get_blank();

if (isset($_POST['submit'])) {
    $id = isset($_POST['id']) ? (int)$_POST['id'] : null;
    if (is_null($id) || !preg_match('/^\d+$/', $id))
        $message = "خطای نامشخصی رخ داد لطفا با پشتیبانی مطرح کنید";
    if ($_POST['submit'] == 'check') { // check done purchases
        $purchase = Purchase::find_by_id($id);
        if ($purchase->check()) {
            $message = "با موفقیت تحویل شد";
        } else {
            $message = "ویرایش بانک اطلاعاتی با مشکل مواجه شد،"
                ." لطفا با پشتیبانی ما در میان بگذارید: ali.zakeri2008@gmail.com<br/>"
                .$db->get_error();
        }
    } elseif ($_POST['submit'] == 'delete') { // delete purchases or papers
        $id = isset($_POST['id']) ? (int)$_POST['id'] : null;
        if (is_null($id) || !preg_match('/^\d+$/', $id))
            $message = "خطای نامشخصی رخ داد لطفا با پشتیبانی مطرح کنید";
        switch ($_POST['what']) {
            case 'purchase':
                $item = Purchase::find_by_id($id);
                break;
            case 'paper':
                $item = Paper::find_by_id($id);
                break;
            default:
                $item = null;
        }
        if (!is_null($item) && $item->remove()) {
            $message = "با موفقیت حذف شد";
        } else {
            $message = "ویرایش بانک اطلاعاتی با مشکل مواجه شد،"
                ." لطفا خطای زیر را با پشتیبانی ما در میان بگذارید: support@autchap.ir<br/>"
                .$db->get_error();
        }
    }
    redirect_to("admin.php", $message);
}
// portal
get_session_message($msg);
include_html_head("Admin", array("admin"));
?>
<body>

<nav id="myNav" class="w3-topnav w3-theme w3-padding w3-card-4" style="direction: rtl;width: 100%;z-index: 99">
    <a class="w3-right" href="index.php"><i class="fa fa-home"></i> خانه</a>
    <a class="w3-right" href="pay.php#all-pays">پرداخت‌ها</a>
    <a class="w3-right" href="orders_list.php">خریدها</a>
    <a class="w3-left" href="login.php?do=logout">خروج <i class="fa fa-sign-out"></i></a>
</nav>
<div class="w3-container w3-theme-dark w3-animate-bottom w3-center w3-bottom" onclick="$(this).slideUp()"
     style="width: 100%;cursor: pointer;z-index: 98;direction: rtl">
    <?= !empty($msg) ? "<p>{$msg}</p>" : "" ?>
</div>

<div class="w3-container w3-padding">
    <div class="w3-card-2 w3-margin-bottom" style="direction: rtl">
        <header class="w3-container w3-theme"><p>سفارش های منتظر</p></header>
        <div class="w3-container w3-padding-8">
            <?php
            $purchases = Purchase::find_by_cond(Purchase::FIELD_STATUS."='".Purchase::STATUS_WAITING."'");
            if ($purchases):
                ?>
                <ul class="w3-ul">
                    <?php
                    foreach ($purchases as $purchase):
                        $book = Paper::find_by_id($purchase->book);
                        ?>
                        <li class="w3-hover-pale-blue w3-padding-right">
                            <form method="post" action="admin.php" class="w3-show-inline-block">
                                <input type="hidden" value="<?= $purchase->id ?>" name="id"/>
                                <input type="hidden" value="purchase" name="what"/>
                                <button class="w3-btn w3-theme-action w3-small" name="submit" value="check">
                                    <i class="fa fa-check"></i></button>
                                <button class="w3-btn w3-theme-action w3-small" name="submit" value="delete"
                                        onclick="confirm('مطمئنی؟')">
                                    <i class="fa fa-times"></i></button>
                            </form>
                            <span class="w3-tag"><?= $purchase->status ?></span>
                            <span class="w3-badge w3-transparent w3-text-black w3-small">
								<?= $book->type == Paper::TYPE_BOOK ? "کتاب" : "جزوه" ?>
							</span>
                            <?= "{$book->title} - {$book->writer}" ?>
                            <span class="w3-tag w3-green"><?= $purchase->paid ?> تومان</span>
                            <span class="w3-hide-large">
								<span class="w3-badge w3-green"><?= $purchase->quantity ?></span>
								<span class="w3-theme-action w3-padding-4"><?= $purchase->auth ?></span>
								<span class="w3-blue-gray w3-padding-4"><?= $purchase->mobile ?></span>
                                <?php if ($purchase->simi): ?>
                                    <span class="w3-tag w3-theme-action"><i class="fa fa-link"></i></span>
                                <?php endif; ?>
                                <span class="w3-small"><?= $purchase->date ?></span>
							</span>
                            <div class="w3-left w3-hide-small w3-hide-medium">
                                <?php if ($purchase->simi): ?>
                                    <span class="w3-tag w3-theme-action"><i class="fa fa-link"></i></span>
                                <?php endif; ?>
                                <span class="w3-badge w3-green"><?= $purchase->quantity ?></span>
                                <span class="w3-theme-action w3-padding-4"><?= $purchase->auth ?></span>
                                <span class="w3-blue-gray w3-padding-4"><?= $purchase->mobile ?></span>
                                <span class="w3-small"><?= $purchase->date ?></span>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?> <p>در حال حاضر سفارش منتظر بررسی وجود ندارد.</p> <?php endif; ?>
        </div>
    </div>
    <div class="w3-card-2 w3-margin-bottom">
        <header class="w3-theme w3-container">
            <a class="w3-btn-floating w3-theme-action w3-left w3-margin" href="#newpaper">
                <i class="fa fa-plus w3-text-theme"></i></a>
            <p class="w3-right-align w3-col s8">لیست جزوات</p>
        </header>

        <div class="w3-container" style="direction: rtl">
            <?php
            $papers = Paper::find_all();
            if ($papers):
                ?>
                <div>
                    <ul class="w3-ul w3-padding">
                        <?php foreach ($papers as $paper): ?>
                            <?php if ($paper->type == Paper::TYPE_PAPER): ?>
                                <li class="w3-hover-pale-blue">
                                    <form action="admin.php" method="post" class="w3-show-inline-block">
                                        <input type="hidden" name="what" value="paper"/>
                                        <input type="hidden" name="id" value="<?= $paper->id ?>"/>
                                        <button class="w3-btn w3-theme-action" name="submit" value="delete"
                                                onclick="confirm('مطمئنی؟')">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>
                                    <form action="admin.php#newpaper" method="post" class="w3-show-inline-block">
                                        <input type="hidden" name="id" value="<?= $paper->id ?>"/>
                                        <button class="w3-btn w3-theme-action" name="submit" value="edit">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </form>
                                    <span class="w3-tag w3-small"><?= $paper->get_paper_size() ?></span>
                                    <?= "{$paper->title} ({$paper->field}) - {$paper->writer}" ?>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php else: ?> <p>هیچ جزوه یا کتابی یافت نشد</p> <?php endif; ?>
        </div>
    </div>
    <div class="w3-card-2 w3-margin-bottom">
        <header class="w3-theme w3-container"><p class="w3-right-align">
                <?= is_numeric($edit_paper->id) ? "ویرایش" : "افزودن" ?> جزوه</p></header>
        <form id="newpaper" method="post" enctype="multipart/form-data" class="w3-form w3-container"
              action="<?= is_numeric($edit_paper->id) ? "edit_paper.php" : "new_paper.php" ?>" style="direction: rtl">
            <input type="hidden" name="id" value="<?= $edit_paper->id ?>"/>
            <input type="hidden" name="MAX_FILE_SIZE" value="<?= 2 * MB_IN_BYTES - KB_IN_BYTES; ?>"/>
            <div class="w3-half">
                <?php if (!is_numeric($edit_paper->id)): ?>
                    <div class="w3-row w3-padding w3-right-align">
                        <label class="w3-label w3-validate" for="pic">تصویر</label>
                        <input class="w3-input w3-border-0" type="file" id="pic" name="pic" accept="image/*" required/>
                    </div>
                <?php endif; ?>
                <div class="w3-row w3-padding w3-right-align">
                    <label class="w3-label" for="title">عنوان</label>
                    <input class="w3-input w3-border" type="text" id="title" name="title"
                           value="<?= $edit_paper->title ?>" required/>
                </div>
                <div class="w3-row w3-padding w3-right-align">
                    <label class="w3-label" for="field">رشته</label>
                    <input class="w3-input w3-border" type="text" id="field" name="field"
                           value="<?= $edit_paper->field ?>" required/>
                </div>
                <div class="w3-row w3-padding w3-right-align">
                    <label class="w3-label" for="desc">توضیحات</label>
                    <textarea class="w3-input w3-border" type="text" id="desc" name="desc" required><?=
                        $edit_paper->desc ?></textarea>
                </div>
            </div>
            <div class="w3-half">
                <div class="w3-row w3-padding w3-right-align">
                    <label class="w3-label" for="pages">تعداد صفحات</label>
                    <input class="w3-input w3-border" min="0" type="number" id="pages" name="pages"
                           value="<?= $edit_paper->pages ?>" required/>
                </div>
                <div class="w3-row w3-padding w3-right-align">
                    <label class="w3-label" for="writer">مولف</label>
                    <input class="w3-input w3-border" type="text" id="writer" name="writer"
                           value="<?= $edit_paper->writer ?>" required/>
                </div>
                <div class="w3-row w3-padding w3-right-align">
                    <label class="w3-label" for="addition">هزینه مازاد</label>
                    <input class="w3-input w3-border" type="number" id="addition" name="additionprice"
                           value="<?= $edit_paper->added_price ?>" min="0" required/>
                </div>
                <div class="w3-row w3-padding w3-right-align">
                    <?php
                    $edit_paper_size = array(
                        "isA4" => ($edit_paper->size == Paper::SIZE_A4 ? "checked" : ""),
                        "isA5" => ($edit_paper->size == Paper::SIZE_A5 ? "checked" : "")
                    );
                    ?>
                    <div class="w3-half">
                        <input class="w3-radio w3-hide" type="radio" id="typebook" name="type" disabled />
                        <label class="w3-validate w3-hide" for="typebook">کتاب</label>
                        <input class="w3-radio" type="radio" id="typepaper" name="type" value="<?= Paper::TYPE_PAPER ?>"
                            checked/>
                        <label class="w3-validate" for="typepaper">جزوه</label>
                    </div>
                    <div class="w3-half" style="direction: ltr">
                        <input class="w3-radio" type="radio" id="a4size" name="papersize" value="<?= Paper::SIZE_A4 ?>"
                            <?= $edit_paper_size['isA4'] ?>/>
                        <label class="w3-validate" for="a4size">A4</label>
                        <input class="w3-radio" type="radio" id="a5size" name="papersize" value="<?= Paper::SIZE_A5 ?>"
                            <?= $edit_paper_size['isA5'] ?>/>
                        <label class="w3-validate" for="a5size">A5</label>
                    </div>
                </div>
                <div class="w3-row w3-padding w3-right-align">
                    <button class="w3-btn w3-input w3-theme-action" name="submit" value="sumbit">
                        <?= is_numeric($edit_paper->id) ? "ذخیره" : "ثبت" ?>
                    </button>
                    <?php if (is_numeric($edit_paper->id)): ?>
                        <a href="" class="w3-btn w3-input w3-theme-action w3-margin-top">جدید</a>
                    <?php endif; ?>
                </div>
            </div>
        </form>
    </div>
    <div class="w3-card-2 w3-margin-bottom">
        <header class="w3-theme w3-container w3-right-align"><p>تنظیمات</p></header>
        <div id="loading" class="w3-container w3-theme-action" onclick="$(this).slideUp()"
             style="direction: rtl;display: none;cursor: pointer;"></div>
        <div class="w3-container w3-form w3-right-align">
            <div class="w3-quarter w3-padding">
                <label class="w3-label" for="paperprice">قیمت کاغذ</label>
                <input id="paperprice" class="w3-input w3-border" name="paperprice" type="number" min="1"
                       value="<?= $options->get_price(Options::ITEM_PRICE_PAPER) ?>" pattern="[1-9][0-9]*" required/>
            </div>
            <div class="w3-quarter w3-padding">
                <label class="w3-label" for="additionprice">هزینه مازاد</label>
                <input id="additionprice" class="w3-input w3-border" name="additionprice" type="number" min="1"
                       value="<?= $options->get_price(Options::ITEM_PRICE_ADDITION) ?>" pattern="[1-9][0-9]*" required/>
            </div>
            <div class="w3-quarter w3-padding">
                <label class="w3-label" for="simiprice">هزینه سیمی</label>
                <input id="simiprice" class="w3-input w3-border w3-margin-bottom" name="simiprice" type="number"
                       min="1" value="<?= $options->get_price(Options::ITEM_PRICE_SIMI) ?>" pattern="[1-9][0-9]*" required/>
            </div>
            <div class="w3-quarter w3-padding">
                <label>&nbsp;</label>
                <button class="w3-input w3-btn w3-theme-action" onclick="saveOptions()">ذخیره</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>