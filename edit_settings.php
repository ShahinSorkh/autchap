<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";

if (isset($_POST['submit']) && $_POST['submit'] == "submit") {
    $paperprice = isset($_POST['paperprice'])
        ? filter_var($_POST['paperprice'], FILTER_VALIDATE_INT) : false;
    $additionprice = isset($_POST['additionprice'])
        ? filter_var($_POST['additionprice'], FILTER_VALIDATE_INT) : false;
    $simiprice = isset($_POST['simiprice'])
        ? filter_var($_POST['simiprice'], FILTER_VALIDATE_INT) : false;

    if ($paperprice === false)
        $errors[] = "برای قیمت کاغذ مقدار مجاز وارد کنید.";
    if ($additionprice === false)
        $errors[] = "برای هزینه مازاد مقدار مجاز وارد کنید.";
    if ($simiprice === false)
        $errors[] = "برای قیمت سیمی مقدار مجاز وارد کنید.";

    if (isset($errors) && !empty($errors)) {
        echo "<ul class=\"w3-ul\"><li>".join("</li><li>", $errors)."</li></ul>";
        return false;
    }

    $out = "<p>";
    $out .= $options->set_prices($additionprice, $paperprice, $simiprice)?
        "تنظیمات با موفقیت ذخیره شد." : "ذخیره تنظیمات با اشکال مواجه شد.";
    $out .= "</p>";
    echo $out;
    return true;
} else
    return false;
