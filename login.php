﻿<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";
// get posted data if there is any
$username = isset($_POST['username']) ? trim($_POST['username']) : null;
$password = isset($_POST['password']) ? trim($_POST['password']) : null;
// basic implementations
$failed_attempts = (int)Log::count_failed_logins();
$valid_attempts = 5;
$valid_user = array(
    "ShahinSorkh" => "SH1997so",
    "Mrdehghan75" => "QWERTYUIOPABCD7576"
);
// logout
if (isset($_REQUEST['do']) && $_REQUEST['do'] === "logout") {
    unset($_SESSION['uid']);
    redirect_to("index.php", "با موفقیت خارج شدید!");
}
// if logged in move to portal
if (isset($_SESSION['uid'])) redirect_to("admin.php");
if ($failed_attempts != 0) {
    $last_attempt = Log::find_last_failed_attempt();
    $fail_waiting_needed = 60 * $failed_attempts * 2; // in sec
    $fail_waiting_elapsed = (time() + 60 * 60 * 3 + 60 * 30) - $last_attempt->timestamp; // in sec
    $fail_waiting_left = $fail_waiting_needed - $fail_waiting_elapsed; // in sec
} else
    $fail_waiting_left = 0;
// on form submit
if (isset($_REQUEST['submit']) && $_REQUEST['submit'] === "submit") {
    if ($failed_attempts % $valid_attempts == 0 && $fail_waiting_left > 0)
        redirect_to("login.php", "لطفا تا پایان یافتن تایمر صبر کنید");

    $username = isset($_POST['username']) ? trim($_POST['username']) : null;
    $password = isset($_POST['password']) ? trim($_POST['password']) : null;

    if (is_null($username) || is_null($password) || empty($username) || empty($password))
        redirect_to("login.php", "نام کاربری یا رمز عبور غیر مجاز است");

    foreach ($valid_user as $user => $pass) {
        if ($user === $username && $password === $pass) {
            $_SESSION['uid'] = $user;
            Log::add(Log::ACTION_LOGIN, "username\t=\t{$username}\npassword\t=\t{$password}");
            redirect_to("admin.php");
        }
    }

    // no valid user found
    Log::add(Log::ACTION_LOGIN_FAILED, "username\t=\t{$username}\npassword\t=\t{$password}");
    redirect_to("login.php", "نام کاربری یا رمز عبور غیر مجاز است");
}
// if not logged in show login form
get_session_message($msg);
include_html_head("Login");
?>
<body>

<div class="w3-blue-gray" style="position: fixed;z-index: -99; height: 100%;width: 100%;"></div>

<div class="w3-container w3-theme-dark w3-animate-top w3-center" onclick="$(this).slideUp()" style="cursor: pointer">
    <?= !empty($msg) ? "<p>{$msg}</p>" : "" ?>
</div>
<?php
if ($failed_attempts % $valid_attempts == 0 && $fail_waiting_left > 0):
    $min = floor($fail_waiting_left / 60);
    $sec = $fail_waiting_left % 60;
    ?>
    <div class="w3-container w3-theme-dark w3-center" style="direction: rtl">
        <p><?= $failed_attempts ?> تلاش ناموفق داشتید</p>
        <p id="timer">زمان باقیمانده تا سعی مجدد:<br/>
            "<span id="sec"><?= $sec < 10 && $sec > 0 ? "0".$sec : $sec ?></span>
            '<span id="min"><?= $min < 10 && $min > 0 ? "0".$min : $min ?></span>
        </p>
    </div>
    <script>var min =<?= $min ?>;
        var sec =<?= $sec ?>;
        var interval = setInterval(function () {
            var a = $("#min");
            var b = $("#sec");
            if (--sec < 0) {
                sec = 59;
                min--
            }
            if (sec < 10 && sec >= 0) {
                $(b).text("0" + sec)
            } else {
                $(b).text(sec)
            }
            if (min < 10 && min > 0) {
                $(a).text("0" + min)
            } else {
                $(a).text(min)
            }
        }, 1000);

        setTimeout(function () {
            clearInterval(interval);
            $("#timer").html("دوباره سعی کنید")
        }, (min * 60 + sec) * 1000);</script>
<?php endif; ?>

<div class="w3-card-2 w3-margin-top w3-white center">
    <header class="w3-theme w3-container w3-large w3-center"><p>ورود ادمین</p></header>
    <div class="w3-container w3-form w3-right-align">
        <form method="post" action="login.php">
            <div class="w3-row w3-padding w3-right-align" style="direction: rtl">
                <label class="w3-label" for="username">نام کاربری</label>
                <input type="text" id="username" name="username" value="<?= !is_null($username) ? $username : ""; ?>"
                       class="w3-input w3-animate-input" style="width: 50%;" required/>
            </div>
            <div class="w3-row w3-padding w3-right-align" style="direction: rtl">
                <label class="w3-label" for="password">رمز ورود</label>
                <input type="password" id="password" name="password" class="w3-input w3-animate-input"
                       style="width: 50%;" required/>
            </div>
            <div class="w3-row w3-padding" style="direction: rtl">
                <button type="submit" value="submit" name="submit" class="w3-btn w3-theme-action">ورود</button>
            </div>
        </form>
    </div>
    <footer class="w3-theme w3-container w3-right-align">
        <p class="w3-left-align" style="direction: rtl"><a href="index.php">بازگشت<i
                    class="fa fa-angle-double-left"></i></a></p>
    </footer>
</div>

</body>
</html>