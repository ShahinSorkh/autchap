/*
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

// Open and close the sidenav on medium and small screens
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}

// Change style of top container on scroll
window.onscroll = function () {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        document.getElementById("myTop").classList.add("w3-card-4", "w3-animate-opacity");
        document.getElementById("myIntro").classList.add("w3-show-inline-block");
    } else {
        document.getElementById("myIntro").classList.remove("w3-show-inline-block");
        document.getElementById("myTop").classList.remove("w3-card-4", "w3-animate-opacity");
    }
};

// Accordions
function myAccordion(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += " w3-theme";
    } else {
        x.className = x.className.replace("w3-show", "");
        x.previousElementSibling.className = x.previousElementSibling.className.replace(" w3-theme", "");
    }
}

// Show order form
function showForm(id, ref) {
    var x = document.getElementById("book" + id + "form");
    if (x.style.display == "none") {
        x.style.display = "block";
        ref.innerHTML = "لغو سفارش";
    } else {
        x.style.display = "none";
        ref.innerHTML = "<i class=\"fa fa-shopping-cart\"></i> سفارش";
    }
}

// Mark activate item in navbar
function navSelector(ref) {
    $("#mySidenav").find("a").each(function () {
        $(this).removeClass("w3-light-grey");
    });
    $(ref).addClass("w3-light-grey");
}

// search on search button clicked
function searchBooks(ref) {
    var resultDiv = $("#search_results");
    var navResult = $("#nav_search_result");
    var searchTerm = $(ref).val();
    var action = "get_books.php";
    var data = "s=" + searchTerm;

    $.ajax(action, {
        type: "get",
        data: data,
        dataType: "json",
        beforeSend: function () {
            var loader = '<div class="w3-row cssload-container w3-padding-0 w3-margin-0">'
                + '<ul class="cssload-flex-container"><li><span class="cssload-loading"></span></li></ul></div>';
            $(resultDiv).html(loader);
            $(navResult).html(loader);
        },
        error: function () {
            $(resultDiv).html("متاسفانه جستجو با خطا مواجه شد، دوباره سعی کنید");
            $(navResult).html("متاسفانه جستجو با خطا مواجه شد، دوباره سعی کنید");
        },
        success: function (json) {
            var papers = json.papers;

            var books_cards = "";
            var papers_cards = "";
            var books_lis = "";
            var papers_lis = "";
            var i;
            if (papers && papers.length > 0) {
                papers_lis += "<p class=\"w3-padding w3-theme w3-margin-0 w3-margin-top\">جزوات یافت شده</p>";
                for (i = 0; i < papers.length; i++) {
                    papers_cards += createCard(papers[i], false);
                    papers_lis += createLi(papers[i]);
                }
            } else papers_lis += "<p class=\"w3-padding w3-theme w3-margin-0 w3-margin-top\">جزوه‌ای یافت نشد</p>";

            $(resultDiv).html(books_cards + papers_cards);
            $(navResult).html(books_lis + papers_lis);
        },
        complete: function () {
            $("img.lazy").lazyload({
                threshold: 200,
                effect: "fadeIn"
            });
        }
    });

    /**
     * @param {JSON} jsonObj
     * @returns {string}
     */
    var createLi = function (jsonObj) {
        var outp = "<a";
        outp += " class=\"w3-navitem w3-border-bottom w3-border-theme w3-padding\"";
        outp += " onclick=\"navSelector(this)\"";
        outp += " href=\"#book" + jsonObj.id + "\">";
        outp += jsonObj.title + " - " + jsonObj.writer;
        outp += "</a>";
        return outp;
    };

    /**
     * @param {JSON} jsonObj
     * @param {bool} isBook
     * @returns {string}
     */
    var createCard = function (jsonObj, isBook) {
        var outp = '<div class="w3-card-2 w3-right-align w3-animate-right w3-margin-bottom" id="book' + jsonObj.id + '">';
        // card header
        outp += '<header class="w3-theme w3-container w3-large"><p>' + jsonObj.title + ' (' + jsonObj.field + ')</p>'
            + '</header>';
        // card content
        outp += '<div class="w3-container w3-padding-4" style="direction: rtl;"><div class="w3-quarter w3-padding">'
            + '<img class="w3-image w3-hover-shadow lazy" data-original="uploads/pic/' + jsonObj.pic
            + '" alt="' + jsonObj.title + '" style="width: 250px;" /></div>';
        outp += '<p class="w3-half w3-padding w3-justify">' + jsonObj.description + '</p>';
        var liClasses = 'w3-border-right w3-border-bottom w3-hover-border-blue-grey';
        outp += '<ul class="w3-ul w3-quarter w3-padding">' + '<li class="' + liClasses + '">قیمت: <span id="p' + jsonObj.id
            + '">' + jsonObj.price + '</span> تومان</li>' + '<li class="' + liClasses + '">مولف: ' + jsonObj.writer
            + '</li>' + '<li class="' + liClasses + '">تعداد صفحه: ' + jsonObj.total_pages + '</li>' + '</ul></div>';
        // order form
        outp += '<div id="book' + jsonObj.id + 'form" class="w3-container w3-padding-4 forms w3-animate-right"' +
            ' style="display: none;"><form method="post" action="order.php" target="_blank">';
        outp += '<input type="hidden" name="bookid" value="' + jsonObj.id + '" />';
        outp += '<div class="w3-third w3-padding">' + '<label class="w3-label" for="mobile' + jsonObj.id + '">همراه</label>'
            + '<input type="text" name="mobile" id="mobile' + jsonObj.id + '" dir="ltr" class="w3-input"'
            + ' pattern="09[0-9]{9}" value="09" style="text-align: left" required /></div>';
        outp += '<div class="w3-third w3-padding"><label class="w3-label" for="quantity' + jsonObj.id + '">تعداد</label>'
            + '<input type="number" min="1" name="quantity" id="quantity' + jsonObj.id + '" value="1"'
            + ' class="w3-input" oninput="calculatePrice(' + jsonObj.id + ')" required /></div>';
        outp += '<div class="w3-third w3-padding">';
        outp += '<div class="w3-half w3-margin-bottom"><input type="checkbox" name="simi" id="simi' + jsonObj.id +
            '" class="w3-check" onclick="calculatePrice(' + jsonObj.id + ')" name="simi"';
        outp += isBook ? 'checked disabled' : '';
        outp += '/><label class="w3-validate" for="simi' + jsonObj.id + '">سیمی</label>' + '</div>';
        outp += '<div class="w3-half w3-margin-bottom"><span class="w3-tag w3-green" id="fp' + jsonObj.id + '">'
            + (parseInt(jsonObj.price) + parseInt((isBook ? simiPriceIs : 0))) + '</span></div>';
        outp += '<button class="w3-theme-action w3-hover-blue-grey w3-btn w3-input"'
            + ' type="submit" name="submit" value="submit">ارسال <i class="fa fa-check"></i></button>';
        outp += '</div></form></div>';
        // card footer
        outp += '<footer class="w3-theme w3-container w3-padding-8">'
            + '<button class="w3-left w3-theme-action w3-hover-blue-grey w3-btn"' + ' onclick="showForm(' + jsonObj.id
            + ', this)"><i class="fa fa-shopping-cart"></i> سفارش</button>';
        outp += '<a href=\"#howtoorder\" class=\"w3-theme-action w3-btn w3-left w3-hover-blue-grey w3-margin-left\">'
            + '<i class=\"fa fa-question-circle w3-large\"></i></a></footer>';
        outp += '</div>';
        return outp;
    };
}

// price calculator
function calculatePrice(id) {
    var finalPrice = $("#fp" + id);
    var purePrice = parseInt($("#p" + id).text());
    var quantity = parseInt($("#quantity" + id).val());

    var simi = $("#simi" + id).is(":checked") ? simiPriceIs : 0;
    var ppp = purePrice + simi; // price per paper

    $(finalPrice).text(ppp * quantity);
}

// set lazy load on images
$(document).ready(function () {
    $("img.lazy").lazyload({
        threshold: 200,
        effect: "fadeIn"
    });
});

// show about modal
function getAbout($switch) {
    var box = $("#about");
    var title = $("#about-title");
    var content = $("#about-content");
    var loader = '<div class="w3-row cssload-container w3-padding-0 w3-margin-0">'
        + '<ul class="cssload-flex-container"><li><span class="cssload-loading"></span></li></ul></div>';
    var action = 'about.php';

    $(box).show();

    switch ($switch) {
        case 'contact':
            $(title).text('تماس با ما');
            break;
        case 'about-us':
            $(title).text('درباره ما');
            break;
        case 'terms-of-use':
            $(title).text('قوانین و مقررات');
            break;
        case 'complains':
            $(title).text('ثبت شکایات');
            break;
        default:
            $(box).hide();
            return;
    }

    $.ajax(action, {
        type: 'get',
        data: 'switch=' + $switch,
        async: false,
        beforeSend: function () {
            $(content).html(loader);
        },
        success: function (result) {
            if (result == 'close();')
                $(box).hide();
            $(content).html(result);
        },
        error: function (xhr, status, error) {
            $(content).html(error);
            // $(box).hide();
        },
        complete: function () {
            if ($switch == 'terms-of-use')
                $(box).hide();
        }
    });
}
