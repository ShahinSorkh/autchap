/*
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

// stick navbar on top
window.onscroll = function () {
    var navbar = $("#myNav");
    var firstCard = $(".w3-card-2:first");

    if (document.body.scrollTop > $(navbar).height()
        || document.documentElement.scrollTop > $(navbar).height()) {
        $(navbar).css("position", "fixed");
        $(firstCard).css("marginTop", $(navbar).height() * 4);
    } else {
        $(navbar).css("position", "static");
        $(firstCard).css("marginTop", 16);
    }
};

// save options
function saveOptions() {
    var pricePaper = parseInt($("#paperprice").val());
    var priceAddition = parseInt($("#additionprice").val());
    var priceSimi = parseInt($("#simiprice").val());

    var msgBox = $("#loading");

    if (isNaN(priceAddition) || priceAddition < 0
        || isNaN(pricePaper) || pricePaper < 0
        || isNaN(priceSimi) || priceSimi < 0) {
        $(msgBox).html("<p>قیمت ها را به عدد و صحیح وارد کنید</p>").slideDown();
        return false;
    }

    var data = "submit=submit";
    data += "&paperprice=" + pricePaper;
    data += "&additionprice=" + priceAddition;
    data += "&simiprice=" + priceSimi;

    $.ajax("edit_settings.php", {
        method: "post",
        data: data,
        beforeSend: function () {
            $(msgBox).html("<p>درحال ذخیره...</p>");
            $(msgBox).show();
        },
        success: function (result) {
            $(msgBox).html(result);
        },
        error: function (xhr, status, error) {
            $(msgBox).html("<p>" + error + "</p>");
        }
    });
}
