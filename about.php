<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */
require_once __DIR__ . "/include/init.php";
session_write_close();

$switch = isset($_GET['switch']) ? $_GET['switch'] : 'close();';

switch ($switch) {
    ////////////// contact //////////////////////////////////////////
    case 'contact': ?>
        <div class="w3-half w3-container">
            <p class="w3-theme-action w3-container">پست الکترونیک</p>
            <div class="w3-left-align" style="direction: ltr">support@autchap.ir</div>
            <p class="w3-theme-action w3-container">نشانی پستی</p>
            تهران ، تهران ، میدان سپاه ، خیابان سرباز ، کوچه هجدهم ، پلاک هفت ، واحد یک
        </div>
        <div class="w3-half w3-container">
            <p class="w3-theme-action w3-container">تلفن تماس</p>
            <div class="w3-left-align" style="direction: ltr">
                0938 141 4802
                <br/>
                021 7760 2018
                <br/>
                لطفا بین ساعات 9 الی 16 تماس بگیرید.
            </div>
        </div>
        <?php
        break; // contact
    ////////////// about-us /////////////////////////////////////////
    case 'about-us': ?>
        <img style="width: 60px;float: right;" src="img/logo.png" alt="AUT CHAP"/>
        سایت AUT CHAP از بهمن ماه سال ۱۳۹۵ با موضوع فروش جزوات درسی در دانشگاه صنعتی امیرکبیر راه اندازی گردیده و هدف اصلی آن
        در دسترس قرار دادن جزوات درسی با قیمت پایین تر است.
        <br/>
        تمامی حقوق و مزایای این سایت متعلق
        به محمدرضا دهفان به عنوان بنیان گذار و صاحب امتیاز این سایت می باشد.
        <?php
        break; // about-us
    ////////////// terms-of-use /////////////////////////////////////
    case 'terms-of-use':
        $file = __DIR__."/docs/terms of use.pdf";
        header('Content-Disposition: attachment; filename="AUT CHAP terms of use.pdf"');
        header('Content-type: application/octet-stream');
        echo file_get_contents($file);
        return; // terms-of-use
    ////////////// complains ////////////////////////////////////////
    case 'complains': ?>
        <form class="w3-form" onsubmit="return false;">
            <div class="w3-half w3-padding-left">
                <input type="text" class="w3-input w3-border" id="complain-name" placeholder="نام و نام خانوادگی"
                       required/>
                <br/>
            </div>
            <div class="w3-half">
                <input type="text" class="w3-input w3-border" id="complain-mobile" pattern="^09[0-9]{9}$"
                       placeholder="تلفن همراه" required/>
                <br/>
            </div>
            <input type="text" class="w3-input w3-border" id="complain-subject" placeholder="موضوع"
                   required/>
            <br/>
            <textarea class="w3-input w3-border" id="complain-text" placeholder="متن شکایت" required></textarea>
            <br/>
            <button class="w3-btn w3-theme-action w3-input" id="submit-complain">ارسال</button>
        </form>
        <div id="complain-res-box" class="w3-green w3-container"><p id="complain-result"></p></div>
        <script>
            $('#complain-result').parent().hide();
            $("#submit-complain").click(function (e) {
                e.preventDefault();

                var resultBox = $('#complain-result');
                var name = $('#complain-name');
                var mobile = $('#complain-mobile');
                var subject = $('#complain-subject');
                var text = $('#complain-text');
                var errors = 0;
                var regPersian = /^[\u0600-\u06FF\uFB8A\u067E\u0686\u06AF\u200C\u200F\d\s\(\)\\\/\+\-\_\=\*\.\,\،\»\«\!\#\$\%\|\'\"\`\~]+$/;
                var regMobile = /^09[0-9]{9}$/;

                if (!$(name).val().match(regPersian)) {
                    errors += 1;
                    if (!$(name).hasClass('w3-border-red'))
                        $(name).addClass('w3-border-red');
                } else {
                    if ($(name).hasClass('w3-border-red'))
                        $(name).removeClass('w3-border-red');
                }
                if (!$(mobile).val().match(regMobile)) {
                    errors += 10;
                    if (!$(mobile).hasClass('w3-border-red'))
                        $(mobile).addClass('w3-border-red');
                } else {
                    if ($(mobile).hasClass('w3-border-red'))
                        $(mobile).removeClass('w3-border-red');
                }
                if (!$(subject).val().match(regPersian)) {
                    errors += 1;
                    if (!$(subject).hasClass('w3-border-red'))
                        $(subject).addClass('w3-border-red');
                } else {
                    if ($(subject).hasClass('w3-border-red'))
                        $(subject).removeClass('w3-border-red');
                }
                if (!$(text).val().match(regPersian)) {
                    errors += 1;
                    if (!$(text).hasClass('w3-border-red'))
                        $(text).addClass('w3-border-red');
                } else {
                    if ($(text).hasClass('w3-border-red'))
                        $(text).removeClass('w3-border-red');
                }

                if (errors == 0) {
                    var data = 'submit=submit';

                    data += '&name=' + $(name).val();
                    data += '&mobile=' + $(mobile).val();
                    data += '&subject=' + $(subject).val();
                    data += '&text=' + $(text).val();

                    $(resultBox).parent().hide();

                    $.ajax('complain.php', {
                        type: 'post',
                        data: data,
                        beforeSend: function () {
                            $(resultBox).parent().slideDown();
                            $(resultBox).text('لطفا شکیبا باشید...');
                        },
                        error: function (xhr, status, error) {
                            $(resultBox).text(error);
                        },
                        success: function (result) {
                            $(resultBox).html(result);
                        }
                    });
                } else if (errors > 10)
                    $(resultBox).text('شماره همراه معتبر و به زبان فارسی وارد کنید').parent().slideDown();
                else if (errors < 10)
                    $(resultBox).text('به زبان فارسی پر کنید').parent().slideDown();
                else
                    $(resultBox).text('شماره همراه معتبر وارد کنید').parent().slideDown();
            });
        </script>
        <?php
        break; // complains
    ////////////// nothing //////////////////////////////////////////
    default:
        echo 'close();';
}
exit();
