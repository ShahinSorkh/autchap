<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";
// get all papers
$papers = Paper::find_by_cond(Paper::FIELD_TYPE."=".Paper::TYPE_PAPER);
// show html
get_session_message($msg);
include_html_head(null, array("script", "lazyload"));
?>
    <body>

    <script>var simiPriceIs = <?= $options->get_price(Options::ITEM_PRICE_SIMI) ?>;</script>

    <div class="w3-container w3-theme-dark w3-animate-bottom w3-center" onclick="$(this).slideUp()"
         style="position: fixed;bottom: 0;width: 100%;cursor: pointer;z-index: 98;direction: rtl"><?= !empty($msg) ? "<p>{$msg}</p>" : "" ?></div>

    <nav class="w3-sidenav w3-white w3-animate-left w3-card-2 w3-right-align" style="z-index:3;width:250px;"
         id="mySidenav">
        <a href="#" class="w3-padding-0 w3-padding-4 w3-image w3-hover-pale-blue w3-center">
            <img class="center" style="width: 100%;" src="img/logo-h.png" alt="AUT CHAP"/></a>
        <div class="w3-row">
            <input type="text" class="w3-input w3-bottombar w3-topbar w3-border-theme w3-hover-pale-blue"
                   style="direction: rtl" placeholder="جستجو..." id="mSearchInput" oninput="searchBooks(this)"/>
        </div>
        <a href="javascript:void(0)" onclick="w3_close()" class="w3-text-teal w3-hide-large w3-closenav w3-large">
            Close <i class="fa fa-remove"></i></a>
        <div id="nav_search_result">
            <?php if ($papers): ?>
                <p class="w3-theme w3-padding w3-margin-0 w3-margin-top" style="direction: rtl">فهرست جزوات</p>
                <?php foreach ($papers as $paper): ?>
                    <a class="w3-navitem w3-border-bottom w3-border-theme w3-padding" href="#book<?= $paper->id ?>"
                       onclick="navSelector(this)">جزوه <?= "{$paper->title} - {$paper->writer}" ?></a>
                <?php endforeach; ?>
            <?php else: ?>
                <a class="w3-navitem w3-border-bottom w3-border-theme w3-padding w3-margin-top" href="#">
                    هیچ جزوه ای یافت نشد!
                </a>
            <?php endif; ?>
        </div>
        <?php /*
            <div class="w3-accordion">
                <a onclick="myAccordion('demo')" href="javascript:void(0)">Accordions <i class="fa fa-caret-down"></i></a>
                <div id="demo" class="w3-accordion-content w3-animate-left w3-padding">
                    <a href="#">Link 1</a>
                    <a href="#">Link 2</a>
                    <a href="#">Link 3</a>
                </div>
            </div>
	    */ ?>
    </nav>

    <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" id="myOverlay"></div>

    <div class="w3-main" style="margin-left:250px;">

        <div id="myTop" class="w3-top w3-container w3-padding-16 w3-theme w3-large">
            <i class="fa fa-bars w3-opennav w3-hide-large w3-xlarge w3-margin-left w3-margin-right"
               onclick="w3_open()"></i>
            <img src="img/logo-no-padding.png" class="w3-image w3-responsive"
                 style="width: auto;height: 26px;" alt="AUT chap"/>
            <span id="myIntro" class="w3-hide-large">AUT CHAP</span>
            <div class="w3-hide-small w3-show-inline-block">
                <a class="w3-btn w3-small" onclick="getAbout('contact')">تماس با ما</a>
                <a class="w3-btn w3-small" onclick="getAbout('about-us')">درباره ما</a>
                <a class="w3-btn w3-small" href="about.php?switch=terms-of-use" ">قوانین و مقررات</a>
                <a class="w3-btn w3-small" onclick="getAbout('complains')">ثبت شکایات</a>
            </div>
        </div>

        <header class="w3-image w3-padding-24 w3-responsive w3-animate-opacity w3-padding-small">
            <div class="w3-hide-large" style="height: 50px;"></div>
            <div class="w3-hide-medium w3-hide-small" style="height: 50px;"></div>
            <img style="width: 100%;margin-left: auto;margin-right: auto;"
                 src="<?= SITE_HOME ?>/img/poster.jpg?v=2" alt="AUT CHAP"/>
            <h1 class="w3-hide">AUT CHAP</h1>
        </header>

        <div id="content" class="w3-container w3-padding-32" style="padding-left:32px">

            <?php
            if (isset($_SESSION['purchase_done'])) {
                $done_purchase_id = filter_var($_SESSION['purchase_done'], FILTER_VALIDATE_INT);
                if ($done_purchase_id):
                    $done_purchase = Purchase::find_by_id($done_purchase_id);
                    $purchased_paper = Paper::find_by_id($done_purchase->book); ?>
                    <div id="thanks_for_shopping" class="w3-card-2 w3-yellow w3-margin-bottom" style="direction: rtl">
                        <header class="w3-theme w3-container">
                            <p class="w3-large">نتیجه سفارش <span class="w3-tag">سفارش با موفقیت ثبت شد</span></p>
                        </header>
                        <div class="w3-container">
                            <p>
                                <?= $purchased_paper->type == Paper::TYPE_BOOK ? "کتاب" : "جزوه" ?>
                                <span class="w3-tag w3-green"><?= $purchased_paper->title ?></span>
                                نوشته
                                <span class="w3-tag w3-green"><?= $purchased_paper->writer ?></span>
                                هر جلد
                                <span class="w3-tag w3-green"><?= $purchased_paper->calculate_price() ?> تومان</span>
                            </p>
                            <p>
                                تعداد
                                <span class="w3-tag w3-green"><?= $done_purchase->quantity ?></span>
                                سیمی ؟
                                <span class="w3-tag w3-green"><?= $done_purchase->simi ? "بله" : "خیر" ?></span>
                                مجموعا
                                <span class="w3-tag w3-green"><?= $done_purchase->paid ?> تومان</span>
                            </p>
                            <p class="w3-center w3-xlarge">
                                کد رهگیری:
                                <span class="w3-tag w3-green w3-jumbo"><?= $done_purchase->auth ?></span>
                            </p>
                            <p class="w3-large">
                                منتظر تماس ما باشید
                                <br/>
                                در صورت لزوم میتوانید با
                                <a href="mailto:support@autchap.ir">support@autchap.ir</a>
                                تماس بگیرید.
                            </p>
                        </div>
                    </div>
                    <?php
                endif;
                unset($_SESSION['purchase_done']); // I'm done with this session data
            }
            ?>

            <div id="search_results" style="direction: rtl">
                <?php if ($papers): ?>
                    <?php foreach ($papers as $paper): ?>
                        <div class="w3-card-2 w3-right-align w3-animate-right w3-margin-bottom"
                             id="book<?= $paper->id ?>">
                            <header class="w3-theme w3-container w3-large">
                                <p><?= "{$paper->title} ({$paper->field})" ?></p>
                            </header>

                            <div class="w3-container w3-padding-4" style="direction: rtl;">
                                <div class="w3-quarter w3-padding">
                                    <img class="w3-image w3-hover-shadow lazy"
                                         data-original="uploads/pic/<?= $paper->pic ?>"
                                         alt="<?= $paper->title ?>" style="width: 250px;"/>
                                </div>
                                <p class="w3-half w3-padding w3-justify"><?= nl2br($paper->desc) ?></p>
                                <ul class="w3-ul w3-quarter w3-container">
                                    <?php $li_classes = "w3-border-right w3-border-bottom w3-hover-border-blue-grey" ?>
                                    <li class="<?= $li_classes ?>">قیمت: <span id="p<?= $paper->id ?>">
										<?= $paper->calculate_price() ?></span> تومان
                                    </li>
                                    <li class="<?= $li_classes ?>">مولف: <?= $paper->writer ?></li>
                                    <li class="<?= $li_classes ?>">تعداد صفحات: <span id="pages<?= $paper->id ?>">
										<?= $paper->pages ?></span></li>
                                </ul>
                            </div>

                            <div id="book<?= $paper->id ?>form" class="w3-container w3-padding-4 forms w3-animate-right"
                                 style="display: none;">
                                <form method="post" action="order.php" target="_blank">
                                    <input type="hidden" name="bookid" value="<?= $paper->id ?>"/>
                                    <div class="w3-third w3-padding">
                                        <label class="w3-label" for="mobile<?= $paper->id ?>">همراه</label>
                                        <input type="text" name="mobile" id="mobile<?= $paper->id ?>" class="w3-input"
                                               pattern="09[0-9]{9}" value="09" required/>
                                    </div>
                                    <div class="w3-third w3-padding">
                                        <label class="w3-label" for="quantity<?= $paper->id ?>">تعداد</label>
                                        <input type="number" min="1" name="quantity" id="quantity<?= $paper->id ?>"
                                               value="1"
                                               class="w3-input" oninput="calculatePrice(<?= $paper->id ?>)" required/>
                                    </div>
                                    <div class="w3-third w3-padding">
                                        <div class="w3-half w3-margin-bottom">
                                            <input type="checkbox" id="simi<?= $paper->id ?>" class="w3-check"
                                                   onclick="calculatePrice(<?= $paper->id ?>)" name="simi"/>
                                            <label class="w3-validate" for="simi<?= $paper->id ?>">سیمی</label>
                                        </div>
                                        <div class="w3-half w3-margin-bottom">
                                            <span class="w3-tag w3-green"
                                                  id="fp<?= $paper->id ?>"><?= $paper->calculate_price() ?></span>
                                        </div>
                                        <button class="w3-theme-action w3-hover-blue-grey w3-btn w3-input"
                                                type="submit" name="submit" value="submit">
                                            ارسال <i class="fa fa-check"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <footer class="w3-theme w3-container w3-padding-8">
                                <button class="w3-left w3-theme-action w3-hover-blue-grey w3-btn"
                                        onclick="showForm(<?= $paper->id ?>, this)">
                                    <i class="fa fa-shopping-cart"></i> سفارش
                                </button>
                                <?= get_howtoorder_button() ?>
                            </footer>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p class="w3-theme w3-padding">جزوه‌ای یافت نشد.</p>
                <?php endif; ?>
            </div>
            <hr/>
            <?php /*
		<h2>What is W3.CSS?</h2>

		<p>W3.CSS is a modern CSS framework with built-in responsiveness:</p>

		<ul class="w3-theme-border w3-rightbar" style="list-style:none">
			<li>Smaller and faster than other CSS frameworks.</li>
			<li>Easier to learn, and easier to use than other CSS frameworks.</li>
			<li>Uses standard CSS only (No jQuery or JavaScript library).</li>
			<li>Speeds up mobile HTML apps.</li>
			<li>Provides CSS equality for all devices. PC, laptop, tablet, and mobile:</li>
		</ul>
		<br>

		<hr>
		<h2>Easy to Use</h2>
		<div class="w3-container w3-sand w3-leftbar">
			<p><i>Make it as simple as possible, but not simpler.</i><br>
				Albert Einstein</p>
		</div>

		<h2>W3.CSS Web Site Templates</h2>

		<p>We have created some responsive W3CSS templates for you to use.</p>
		<p>You are free to modify, save, share, use or do whatever you want with them:</p>

		<div class="w3-container w3-padding-16 w3-card-2" style="background-color:#eee">
			<h3 class="w3-center">Blog Template</h3>
			<div class="w3-content" style="max-width:800px"><br>
				<div class="w3-row">
					<div class="w3-col m6">
						<a href="tryw3css_templates_blog.htm" target="_blank" class="w3-btn w3-padding-12 w3-dark-grey"
						   style="width:88.5%">Demo</a>
					</div>
					<div class="w3-col m6">
						<a href="w3css_templates.asp" class="w3-btn w3-padding-12 w3-dark-grey" style="width:88.5%">
							More Templates »</a>
					</div>
				</div>
			</div>
		</div>
		<br>

		<h2 class="w3-section w3-text-teal">Colors</h2>
		<p>W3.CSS uses color classes.</p>
		<p>The color classes are inspired by colors used in marketing, road signs, and sticky notes.</p>
		<div class="w3-container w3-red"><p>w3-red</p></div>
		<br>
		<div class="w3-container w3-pink"><p>w3-pink</p></div>
		<br>
		<div class="w3-container w3-purple"><p>w3-purple</p></div>
		<br>
		<div class="w3-container w3-deep-purple"><p>w3-deep-purple</p></div>
		<br>
		<div class="w3-container w3-indigo"><p>w3-indigo</p></div>
		<br>
		<div class="w3-container w3-blue"><p>w3-blue</p></div>
		<br>
		<div class="w3-container w3-light-blue"><p>w3-light-blue</p></div>
		<br>
		<div class="w3-container w3-cyan"><p>w3-cyan</p></div>
		<br>
		<div class="w3-container w3-aqua"><p>w3-aqua</p></div>
		<br>
		<div class="w3-container w3-teal"><p>w3-teal</p></div>
		<br>
		<div class="w3-container w3-green"><p>w3-green</p></div>
		<br>
		<div class="w3-container w3-light-green"><p>w3-light-green</p></div>
		<br>
		<div class="w3-container w3-lime"><p>w3-lime</p></div>
		<br>
		<div class="w3-container w3-sand"><p>w3-sand</p></div>
		<br>
		<div class="w3-container w3-khaki"><p>w3-khaki</p></div>
		<br>
		<div class="w3-container w3-yellow"><p>w3-yellow</p></div>
		<br>
		<div class="w3-container w3-amber"><p>w3-amber</p></div>
		<br>
		<div class="w3-container w3-orange"><p>w3-orange</p></div>
		<br>
		<div class="w3-container w3-deep-orange"><p>w3-deep-orange</p></div>
		<br>
		<div class="w3-container w3-blue-grey"><p>w3-blue-grey</p></div>
		<br>
		<div class="w3-container w3-brown"><p>w3-brown</p></div>
		<br>
		<div class="w3-container w3-light-grey"><p>w3-light-grey</p></div>
		<br>
		<div class="w3-container w3-grey"><p>w3-grey</p></div>
		<br>
		<div class="w3-container w3-dark-grey"><p>w3-dark-grey</p></div>
		<br>
		<div class="w3-container w3-black"><p>w3-black</p></div>
		<br>
		<div class="w3-container w3-pale-red"><p>w3-pale-red</p></div>
		<br>
		<div class="w3-container w3-pale-yellow"><p>w3-pale-yellow</p></div>
		<br>
		<div class="w3-container w3-pale-green"><p>w3-pale-green</p></div>
		<br>
		<div class="w3-container w3-pale-blue"><p>w3-pale-blue</p></div>

 */ ?>

        </div>

        <footer class="w3-container w3-theme w3-padding-" style="padding-left:32px">
            <p class="w3-small">
                All rights reserved &copy; <?= date('Y') ?>
            </p>
            <p class="w3-small w3-text-light-blue">
                Designed and deployed by <a class="w3-hover-text-white" href="mailto:sorkh.shahin@hotmail.com">Ali Zakeri</a>
            </p>
        </footer>
    </div>

    <div id="howtoorder" class="w3-animate-zoom w3-card-4 w3-white w3-modal">
        <header class="w3-theme w3-container"><p class="w3-large">
                مراحل سفارش :
                <a href="##"><i class="fa fa-times w3-left"></i></a>
            </p></header>
        <div class="w3-container w3-modal-content">
            <ol class="w3-ol">
                <li>روی دکمه ی سفارش کلیک کن</li>
                <li>شماره تلفنتو وارد کن</li>
                <li>هر‌ چنتا که میخوای بزن</li>
                <li>اگر میخوای برات سیمی کنیم تیکشو بزن</li>
                <li>روی دکمه ی ارسال کلیک کن</li>
                <li>پرداختتو انجام بده</li>
            </ol>
            <p>کمتر از ۲۴ ساعت با هماهنگی قبلی میفرستیم برات</p>
        </div>
    </div>

    <div id="about" class="w3-animate-zoom w3-card-4 w3-white w3-modal">
        <header class="w3-theme w3-container"><p class="w3-large">
                <span id="about-title"></span>:
                <a style="cursor:pointer;" onclick="$('#about').hide()"><i class="fa fa-times w3-left"></i></a>
            </p></header>
        <div id="about-content" class="w3-container w3-modal-content w3-padding-bottom w3-margin-bottom"></div>
    </div>

    </body>
    </html>
<?php
/**
 * @return string
 */
function get_howtoorder_button()
{
    $out = "<a href=\"#howtoorder\" class=\"w3-theme-action w3-btn w3-left w3-hover-blue-grey w3-margin-left\" "
        ."title=\"چگونه سفارش دهم؟\">";
    $out .= "<i class=\"fa fa-question-circle w3-large\"></i></a>";
    return $out;
}
