<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/../include/init.php";

Log::add(
    Log::ACTION_ERROR_404,
    SITE_HOME.$_SERVER["REQUEST_URI"]
    .(isset($_SERVER["HTTP_REFERER"]) ? PHP_EOL.$_SERVER["HTTP_REFERER"] : "")
);
include_html_head("404 not found");
?>

<body style="direction: rtl">

<header class="w3-container w3-theme"><h1>صفحه موردنظر یافت نشد!</h1></header>

<div class="w3-container w3-center w3-margin-top">
    <div class="w3-quarter">&nbsp;</div>
    <div class="w3-half">
        <img src="<?= SITE_HOME ?>/img/error-404.png" alt="404 NotFound" style="width: 100%;"
             class="w3-padding"/>
        <p class="w3-large">
            درصورت لزوم میتوانید با پشتیبانی ما
            <a href="mailto:support@autchap.ir" style="direction: rtl">support@autchap.ir</a>
            در میان بگذارید.
        </p>
        <a class="w3-btn w3-theme-action w3-margin" href="<?= SITE_HOME ?>">بازگشت</a>
    </div>

    <div class="w3-quarter">
        <img src="<?= SITE_HOME ?>/img/logo.png" alt="AUT chap" style="width: 100%;"/>
    </div>
</div>

<br/><br/><br/><br/>
<hr/>
</body>
</html>