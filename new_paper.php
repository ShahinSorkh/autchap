<?php
/**
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

require_once __DIR__."/include/init.php";

$title       = isset($_POST['title']) ? trim($_POST['title']):null;
$field       = isset($_POST['field']) ? trim($_POST['field']):null;
$writer      = isset($_POST['writer']) ? trim($_POST['writer']):null;
$desc        = isset($_POST['desc']) ? trim($_POST['desc']):null;
$pages       = isset($_POST['pages']) ? (int)trim($_POST['pages']):null;
$type        = isset($_POST['type']) ? (int)trim($_POST['type']):null;
$size        = isset($_POST['papersize']) ? (int)trim($_POST['papersize']):null;
$added_price = isset($_POST['additionprice']) ? (int)trim($_POST['additionprice']):null;

if (is_null($title) || empty($title)) $errors[] = "فیلد عنوان را پر کنید.";
if (is_null($field) || empty($field)) $errors[] = "فیلد درس را پر کنید.";
if (is_null($writer) || empty($writer)) $errors[] = "فیلد مولف را پر کنید.";
if (is_null($desc) || empty($desc)) $errors[] = "فیلد توضیحات را پر کنید.";
if (is_null($pages) || is_nan($pages)) $errors[] = "فیلد صفحات را پر کنید.";
if (is_null($added_price) || is_nan($added_price)) $errors[] = "فیلد هزینه مازاد را پر کنید.";
if (is_null($type) || is_nan($type)) $errors[] = "کتاب یا جزوه؟";
if (is_null($size) || is_nan($size)) $errors[] = "اندازه کاغذ را مشخص کنید.";

if (isset($errors) && !empty($errors))
	redirect_to("admin.php", "<ul class=\"w3-ul w3-padding\"><li>".join("</li><li>", $errors)."</li></ul>");

/**
 * @return string
 */
function generate_random_name() {
	$alphabet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
	$name = "";
	for ($i = 0; $i < 15; $i ++) {
		$name .= substr($alphabet, rand(0, strlen($alphabet)), 1);
	}
	if (Paper::count_records(Paper::FIELD_PIC."='{$name}'") > 0) return generate_random_name();
	return $name;
}

if (isset($_POST['submit'])) {
	// process the form data
	$tmp_file    = $_FILES['pic']['tmp_name'];
	$file_name   = basename($_FILES['pic']['name']);
	$target_file = generate_random_name().".".pathinfo($file_name, PATHINFO_EXTENSION);
	$upload_dir  = __DIR__."/uploads/pic";

	if (!file_exists($upload_dir)) mkdir($upload_dir, 0777, true);

	// there is no file with same name, i've checked before.
	// move_uploaded_file will return false if $tmp_file is not a valid upload file
	// or if it cannot be moved for any other reason
	if (!move_uploaded_file($tmp_file, $upload_dir."/".$target_file)) {
		$error = $_FILES['pic']['error'];
		redirect_to("admin.php", $upload_errors[$error]);
	}
}

// the picture has beed uploaded and saved
// it's time to save data to the databas
$fields = array(
	Paper::FIELD_TITLE,
	Paper::FIELD_FIELD,
	Paper::FIELD_WRITER,
	Paper::FIELD_DESC,
	Paper::FIELD_SIZE,
	Paper::FIELD_PAGES,
	Paper::FIELD_TYPE,
	Paper::FIELD_ADDED_PRICE,
	Paper::FIELD_PIC
);
$query  = "INSERT INTO ".Paper::TABLE_NAME." (".join(",", $fields).") VALUES ('";
$query .= $db->real_escape_string($title)."','";
$query .= $db->real_escape_string($field)."','";
$query .= $db->real_escape_string($writer)."','";
$query .= $db->real_escape_string($desc)."',";
$query .= $db->real_escape_string($size).",";
$query .= $db->real_escape_string($pages).",";
$query .= $db->real_escape_string($type).",";
$query .= $db->real_escape_string($added_price).",'";
$query .= $db->real_escape_string($target_file)."'";
$query .= ")";
if ($db->query($query)) {
	$message = "با موفقیت افزوده شد.";
} else {
	unlink($upload_dir."/".$target_file);
	$message = "ذخیره سازی در بانک اطلاعاتی با اشکال مواجه شد."."<br/>".$db->get_error();
}
redirect_to("admin.php", $message);